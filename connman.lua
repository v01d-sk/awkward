--[[
  Copyright 2016 Stefano Mazzucco <stefano AT curso DOT re>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

local string = string

-- Connman network widget
local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

local ConnectionManager = require("connman_dbus")
local current_service = nil;
local spawn_with_shell = awful.spawn.with_shell or awful.util.spawn_with_shell
local awtk = require("awtk")
local textbox = wibox.widget
{
    markup="",
    align="center",
    valign="center",
    font = awtk.theme.menubar.font.family,
    widget=wibox.widget.textbox,
    forced_width = (awtk.theme.menubar.font.width + 1) * 4,
}
local notify = nil
local separator = require("separator")

local function default_table(t, default_value)
  t = t or {}
  local mt = {
    __index = function (tbl, key)
      if type(default_value) == "table" then
        -- create a new table for each new key
        default_value = {}
      end
      rawset(tbl, key, default_value)
      return default_value
    end
  }
  setmetatable(t, mt)
  return t
end

local icon_statuses = default_table(
  {
    cellular = {
      three_g = "network-cellular-3g",
      four_g = "network-cellular-4g",
      acquiring = "network-cellular-acquiring",
      connected = "network-cellular-connected",
      edge = "network-cellular-edge",
      gprs  = "network-cellular-gprs",
      hspa  = "network-cellular-hspa",
      no_route  = "network-cellular-no-route",
      offline  = "network-cellular-offline",
      signal = {
        excellent = "network-cellular-signal-excellent",
        good = "network-cellular-signal-good",
        none = "network-cellular-signal-none",
        ok = "network-cellular-signal-ok",
        weak = "network-cellular-signal-weak",
      }
    },
    unspecified = {
      err = "network-error",
      idle = "network-idle",
      no_route = "network-no-route",
      offline = "network-offline",
      receive = "network-receive",
      transmit_receive = "network-transmit-receive",
      transmit = "network-transmit",
    },
    vpn = {
      acquiring = "network-vpn-acquiring",
      connected = "network-vpn",
    },
    ethernet = {
      acquiring =  "network-wired-acquiring",
      disconnected = "network-wired-disconnected",
      no_route = "network-wired-no-route",
      offline = "network-wired-offline",
      connected = "network-wired",
    },
    wifi = {
      acquiring = "network-wireless-acquiring",
      connected  = "network-wireless-connected",
      encrypted  = "network-wireless-encrypted",
      hotspot = "network-wireless-hotspot",
      no_route = "network-wireless-no-route",
      offline = "network-wireless-offline",
      signal = {
        excellent = "network-wireless-signal-excellent",
        good = "network-wireless-signal-good",
        ok = "network-wireless-signal-ok",
        weak = "network-wireless-signal-weak",
        none = "network-wireless-signal-none",
      },
    },
  },
  {})

local show_signal = {ready = true, online = true}


local function get_wifi_icon(size, service)
  print("get_wifi_icon")
  local states = {
    idle = "no_route", -- is this correct?
    failure = "offline",
    association = "acquiring",
    configuration = "acquiring",
    disconnect = "offline",
  }
  if show_signal[service.State] then
    local s = service.Strength
    local v
    if s <= 0 then
      v = "none"
    elseif s <= 25 then
      v = "weak"
    elseif s <= 50 then
      v = "ok"
    elseif s <= 75 then
      v = "good"
    else
      v = "excellent"
    end
    local text = awtk.textutils.colorize_number(100, 0, s, false) .. "%"
    textbox:set_markup(text)
    return awtk.theme.get_icon(size, 'panel/' .. icon_statuses.wifi.signal[v])
  else
    local text = awtk.textutils.colorize_number(100, 0, 0, false) .. "%"
    textbox:set_markup(text)
    return awtk.theme.get_icon(size, 'panel/' .. icon_statuses.wifi[states[service.State]])
  end
end

local function get_wired_icon(size, service)
  local states = {
    idle = "no_route", -- is this correct?
    failure = "offline",
    association = "acquiring",
    configuration = "acquiring",
    ready = "connected",
    disconnect = "disconnected",
    online = "connected",
  }
  local fd = io.popen("ip addr | awk '/state UP/ {print $2}' | cut -f1 -d ':'")
  local iface = fd:read("*line")
  textbox:set_markup('<span color="#e78a4e">' .. iface .. '</span>')
  return awtk.theme.get_icon(size, 'panel/'.. icon_statuses.ethernet[states[service.State]])
end

local service_types = {
  ethernet = get_wired_icon,
  wifi = get_wifi_icon
}

local function get_status_icon(manager)
  local current_service = manager.services[1]

  if current_service then
    local f = service_types[current_service.Type]
    if type(f) == "function" then
      return f(24, current_service)
    end
  end
  awtk.theme.get_icon(24, basename(icon_statuses.unspecified[manager.State]) or icon_statuses.unspecified.err)
end

local widget = wibox.widget.imagebox()

function update_tooltip(manager)
  current_service = manager.services[1]
end

local function notify_show(manager)
	local icon = ""
	local msg
	local color
	  if current_service then
	    local f = service_types[current_service.Type]
	    if type(f) == "function" then
	      icon = "panel/" .. awtk.theme.basename(f(128, current_service))
	    end
	    msg =        "--------------------------------\n"
	    msg = msg .. "<span color='white'>        Network connection</span>\n"
        local servicename = current_service.Name
        if (string.len(current_service.Name) <= 16) then
            servicename = awtk.textutils.align_string(current_service.Name,16)
        end
	    msg = msg .. "--------------------------------\n"
	    if current_service.Type == "wifi"  then
	    	msg = msg .."ESSID:\t\t<span color='white'>" .. servicename .. "</span>\n"
	    end
	    if (current_service.Error) then
		    color = awtk.theme.textutils.colors[1]
	    else
		    color = awtk.theme.textutils.colors[4]
	    end
	    msg = msg .. "Status:\t\t<span color=" .. color .. ">" .. awtk.textutils.align_string(current_service.State, string.len(servicename)) .. "</span>\n"
	    if current_service.Type == "wifi" and show_signal[current_service.State] then
		    msg = msg .. "Signal:\t\t" .. awtk.textutils.colorize_number_len(100, 0, current_service.Strength, false, string.len(servicename)-1) .. "%\n" 
	    end
	    local fd = io.popen("ip addr | awk '/state UP/ {print $2}' | cut -f1 -d ':'")
	    local iface = fd:read("*line")
	    fd:close()
	    msg = msg .. "Interface:\t<span color='white'>" .. awtk.textutils.align_string(iface, string.len(servicename)) .. "</span>\n"
        
	    fd = io.popen("ip addr show dev " .. iface .. " | grep 'inet ' | awk '{print $2}' | cut -f1 -d '/'")
	    local ipaddr = fd:read("*line")
	    fd:close()
	    msg = msg .. "IP:\t\t<span color='white'>" .. awtk.textutils.align_string(ipaddr, string.len(servicename)) .. "</span>\n"

	  else
	    tooltip = manager.State
	  end
	notify=awtk.notify.new(icon, msg)
	notify:show(screen.primary.index)
end 

local function notify_hide(manager)
	if (notify ~= nil) then
		notify:hide()
	end
	notify = nil
end
function widget:update(manager)
  self:set_image(get_status_icon(manager))
  update_tooltip(manager)
end


function connman_show()
	if (notify == nil) then
		notify_show()
	else
	    notify_hide()
        end
end

ConnectionManager:connect_signal(
  function (self)
    widget:update(self)
  end,
  "PropertyChanged"
)

ConnectionManager:connect_signal(
  function (self)
    widget:update(self)
  end,
  "ServicesChanged"
)
local function builder()
    widget:update(ConnectionManager)
	local conlayout = wibox.layout.fixed.horizontal() 
    conlayout.spacing = awtk.theme.menubar.spacing
    conlayout:add(widget)
    conlayout:add(textbox)
    conlayout:connect_signal("mouse::enter", function () notify_show() end)
    conlayout:connect_signal("mouse::leave", function () notify_hide() end)
    return conlayout;
end

return builder()
