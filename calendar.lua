local awtk = require("awtk")
local wibox = require ("wibox")
local awful = require("awful")
local ical = require("ical")
local calendar_obj=nil
local keygrabber = require("awful.keygrabber")
local calendar = {}
local curlcmd = "curl"
local separator = require("separator")
local gears = require("gears")
calendar.__index = calendar
local ics_year = {
                    {caption = "Slovakia", url = "https://calendar.zoznam.sk/icalendar/create-vcard-multiple.php?fName=sk&hy=##YEAR##", color = "#330000"},
                    {caption = "US", url = "https://www.officeholidays.com/ics-fed/usa", color = "#000033"}
                 }

function calendar.grabber(self, mod, key, event)
    local tmpmod
    local tmpkey
    if event ~= "press" then return end
    if key == "Right" then
        calendar.inc(self)
    elseif key == "Left" then
        calendar.dec(self)
    elseif #mod == 3 then
        if (mod[2] == "Mod2" and mod[3] == "Mod4") then
            calendar.hide(self)
            if key ~= "d" then
            awesome.emit_signal(event, mod, key)
            end
        end
    end
end

function calendar.process_ics(self, input_data)
    local need_download = true
    if self._ics_data ~= {} then
        for i,data in pairs(self._ics_data) do
            if data.caption == input_data.caption then
                for j, year in pairs(data.years) do
                    if tonumber(year.year) == self._curyear then
                        need_download = false
                    end
                end
            end
        end
    else 
        need_download = true 
    end
    if (need_download) then
        url = input_data.url:gsub("##YEAR##",tostring(self._curyear))
        print("Downloading " .. url)
--		awful.spawn.easy_async (curlcmd .. " --silent " .. url,
--					function (stdout, stderr, reason, exitcode)
--                        icalobj = ical.new(stdout)
--
--                    end)

    end

end

function calendar.decorate_cell(self, widget, flag, date)
    if flag=='monthheader' then
        flag = 'header'
    end    
    if (widget.get_text) then
        tmp = widget:get_text()
    end
    if (flag == "header") then
        local len = 11 + math.floor(string.len(tmp) / 2)
        tmp = awtk.textutils.align_string(tmp, len)
        self._text = "----------------------\n<span color='white'>" .. string.upper(tmp) .. "</span>\n----------------------\n<span color='white'><b> Mo Tu We Th Fr Sa Su</b></span>"
        self._dates = ""
        self._line = ""
    elseif flag == "normal" then
        if string.len(tmp) > 0 then
            local d = {year=date.year, month=(date.month or 1), day=(date.day or 1)}
            local weekday = tonumber(os.date('%w', os.time(d)))
            if (self._line == "" and weekday ~= 1) then
                if (weekday == 0) then
                    pos = 7
                else
                    pos = weekday
                end

                self._line = awtk.textutils.align_string(" ", 3 * (pos - 1))
            end
            local color = (weekday==0 or weekday==6) and 'green' or 'gray'
            self._line = self._line .. "<span color='" .. color .. "'>" .. awtk.textutils.align_string(tmp,3) .. "</span>"
            if (weekday == 0) then
                if string.len(self._line) < 22 then
                    self._line = awtk.textutils.align_string(self._line, 22)
                end
                self._dates = self._dates .. self._line .. "\n"
                self._line = ""
            end
        else
            self._line = self._line .. "   "
        end
    elseif flag == "focus" then
        if string.len(tmp) > 0 then
            local d = {year=date.year, month=(date.month or 1), day=(date.day or 1)}
            local weekday = tonumber(os.date('%w', os.time(d)))
            if (self._line == "" and weekday ~= 1) then
                if (weekday == 0) then
                    pos = 7
                else
                    pos = weekday
                end

                self._line = awtk.textutils.align_string(" ", 3 * (pos - 1))
            end
            local color = (weekday==0 or weekday==6) and 'green' or 'grey'
            self._line = self._line .. " <span color='black' background='"..color.."'>" .. awtk.textutils.align_string(tmp,2) .. "</span>"
            if (weekday == 0) then
                self._dates = self._dates .. self._line .. "\n"
                self._line = ""
            end
        end
    end
    return wibox.widget {}
end

function calendar.processdata(self)
    local day = 0

    if (self._curmonth == tonumber(os.date("%m")) and
        self._curyear == tonumber(os.date("%Y"))) then
        showdate = os.date('*t')
    else
        showdate = {
            month = self._curmonth,
            year = self._curyear
        }
    end
    for i,input in pairs(ics_year) do
        self:process_ics(input)
    end
    local cal = wibox.widget {
        date     = showdate,
        fn_embed = function(widget, flag, date) return calendar_obj:decorate_cell(widget, flag, date) end,
        widget   = wibox.widget.calendar.month
    }
    if (self.notify == nil) then
        self.notify = awtk.notify.new("apps/calendar",self._text .. '\n' .. self._dates .. self._line .. '\n')
    else
        self.notify:update_text(self._text .. '\n' .. self._dates .. self._line .. '\n')
    end

end

function calendar.datewidget(self)
	return self._datelayout
end

function calendar.clockwidget(self)
    return self._clocklayout
end

function calendar.readdata(self)
    fd = io.popen("cal -m " .. self._curmonth .. " " .. self._curyear)
    self.lines={}
    status = fd:read("*line")
    while (status ~= nil) do
        table.insert(self.lines,status)
        status = fd:read("*line")
    end
    fd:close()
end

function calendar.show(self)
    if (self.visible == false) then
        self.visible=true
        self:processdata()
		self.notify:show()
    	keygrabber.run(self._keygrabber)
    else
        self:hide()
    end
end

function calendar.hide(self)
    if (self.visible == true) then
    	self._curmonth = tonumber(os.date("%m"))
		self._curyear = tonumber(os.date("%Y"))
        self.visible=false
        self.notify:hide()
    	keygrabber.stop(self._keygrabber)
    end
end

function calendar.update(self)
    self:processdata()
end

function calendar.inc(self)
	self._curmonth = self._curmonth + 1
	if (self._curmonth > 12 ) then
		self._curmonth = 1
		self._curyear = self._curyear + 1
	end
	self:update()
end

function calendar.dec(self)
	self._curmonth = self._curmonth - 1
	if (self._curmonth < 1 ) then
		self._curmonth = 12
		self._curyear = self._curyear - 1
	end
	self:update()
end

function calendar.new()
    local self = setmetatable({},calendar)
    self._text = ""
    self._dates = ""
    self._line = ""
    self._ics_data = {} 
	self._datelayout = wibox.layout.fixed.horizontal()
	self._clocklayout = wibox.layout.fixed.horizontal()
	self._date = wibox.widget.textclock("<span color='#51a0cf'>%m-%d</span>")
    self._date.font = awtk.theme.menubar.font.family
	self._clock = wibox.widget.textclock("<span color='#51a0cf'>%H:%M</span>") 
    self._clock.font = awtk.theme.menubar.font.family
    self.notify = nil
    self.visible = false
    self._curmonth = tonumber(os.date("%m"))
	self._curyear = tonumber(os.date("%Y"))
    self.lines={}
    self._keygrabber = function (...)
                            calendar.grabber(self, ...)
                       end    
	local cal_icon=wibox.widget.imagebox()	
	local clock_icon=wibox.widget.imagebox()	
	cal_icon:set_image(awtk.theme.get_icon(24,"panel/calendar"))
	clock_icon:set_image(awtk.theme.get_icon(24,"panel/clock"))
	self._datelayout:add(cal_icon)
    self._datelayout:add(self._date)
    self._datelayout.spacing = awtk.theme.menubar.spacing
    datebg = wibox.container.background()
    datebg.bg = awtk.theme.menubar.color.bg
    datebg.widget = datelayout
    self._clocklayout:add(clock_icon)
    self._clocklayout:add(self._clock)
    self._clocklayout.spacing = awtk.theme.menubar.spacing

	self._datelayout:connect_signal("mouse::enter", function () calendar.show(self) end)
	self._datelayout:connect_signal("mouse::leave", function () calendar.hide(self) end)
	self._datelayout:buttons(awful.util.table.join(
    	awful.button({ }, 4, function () calendar.dec(self) end),
    	awful.button({ }, 5, function () calendar.inc(self) end)
	))
	
    return self
end
calendar_obj = calendar:new()
return calendar_obj
