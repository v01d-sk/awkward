#!/usr/bin/lua
local awtk = require("awtk")
local awful = require("awful")
local json = require("json")

local weatherutil={}
local weather_obj = nil
local update_cycle = 18000 --seconds
local curlcmd = "curl"
local location=298738
local city="Košice"
local location_by_ip=false
local accuweather_api_key="WmWjym5zBMQT2JV9NvViXBmkkKtyMnXY"
local metric=true

weatherutil.__index = weatherutil
local font = {
   {"  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  "
   },
   {"    ▉▉  ",
    "   ▉▉▉  ",
    "  ▉▉▉▉  ",
    " ▉▉ ▉▉  ",
    "    ▉▉  ",
    "    ▉▉  ",
    "  ▉▉▉▉▉ "
   },
   {"  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    "    ▉▉  ",
    "   ▉▉   ",
    "  ▉▉    ",
    " ▉▉     ",
    " ▉▉▉▉▉▉ "
   },
   {"  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    "     ▉▉ ",
    "   ▉▉▉  ",
    "     ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  "
   },
   {" ▉▉     ",
    " ▉▉     ",
    " ▉▉     ",
    " ▉▉  ▉▉ ",
    " ▉▉▉▉▉▉ ",
    "     ▉▉ ",
    "     ▉▉ "
   },
   {" ▉▉▉▉▉▉ ",
    " ▉▉     ",
    " ▉▉     ",
    "  ▉▉▉▉  ",
    "     ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  "
   },
   {"  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    " ▉▉     ",
    " ▉▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  "
   },
   {" ▉▉▉▉▉▉ ",
    "     ▉▉ ",
    "     ▉▉ ",
    "    ▉▉  ",
    "   ▉▉   ",
    "  ▉▉    ",
    " ▉▉     "
   },
   {"  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  "
   },
   {"  ▉▉▉▉  ",
    " ▉▉  ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉▉ ",
    "     ▉▉ ",
    " ▉▉  ▉▉ ",
    "  ▉▉▉▉  "
   },
   {"        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        "
   },
   {"        ",
    "        ",
    "        ",
    " ▉▉▉▉▉▉ ",
    "        ",
    "        ",
    "        "
   }
}
local celsius_unit =
   {"  ▉▉▉▉    ▉▉▉▉  ",
    " ▉▉  ▉▉  ▉▉  ▉▉ ",
    " ▉▉  ▉▉  ▉▉     ",
    "  ▉▉▉▉   ▉▉     ",
    "         ▉▉     ",
    "         ▉▉  ▉▉ ",
    "          ▉▉▉▉  "
   };

local farenheit_unit =
   {"     ▉▉▉▉▉▉     ",
    "     ▉▉         ",
    "     ▉▉         ",
    "     ▉▉▉▉       ",
    "     ▉▉         ",
    "     ▉▉         ",
    "     ▉▉         "
   };

local icon_cloudy = {
    big = {
        "                        ",
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>   .-(     (        ).  </span>",
        "<span color='grey'>  (_______.______)____) </span>",
        "                        "
    },
    small = {
        "              ",
        "<span color='grey'>     .--.     </span>",
        "<span color='grey'>  .-(    ).   </span>",
        "<span color='grey'> (___.__)__)  </span>",
        "              "
    }
}

local icon_light_rain = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "<span color='blue'>      ‘    ‘    ‘   ‘   </span>",
        "<span color='blue'>       ‘    ‘    ‘      </span>",
        "<span color='blue'>    ‘    ‘    ‘   ‘     </span>"
    },
    small = {
        "<span color='grey'>     .-.      </span>",
        "<span color='grey'>    (   ).    </span>",
        "<span color='grey'>   (___(__)   </span>",
        "<span color='blue'>    ‘ ‘ ‘ ‘   </span>",
        "<span color='blue'>   ‘ ‘ ‘ ‘    </span>"
    }
}


local icon_hail = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "<span color='white'>      &#9671;    &#9671;    &#9671;   &#9671;   </span>",
        "<span color='white'>       &#9671;    &#9671;    &#9671;      </span>",
        "<span color='white'>    &#9671;    &#9671;    &#9671;   &#9671;     </span>"
    },
    small = {
        "<span color='grey'>     .-.      </span>",
        "<span color='grey'>    (   ).    </span>",
        "<span color='grey'>   (___(__)   </span>",
        "<span color='white'>    &#9671; &#9671; &#9671; &#9671;   </span>",
        "<span color='white'>   &#9671; &#9671; &#9671; &#9671;    </span>"
    }
}
local icon_sunny =  {
    big = {
        "<span color='yellow'>          \\     /       </span>",
        "<span color='yellow'>           \\   /        </span>",
        "<span color='yellow'>            .-.         </span>",
        "<span color='yellow'>    ―――――  (   )  ――――― </span>",
        "<span color='yellow'>            `-’         </span>",
        "<span color='yellow'>           /   \\        </span>",
        "<span color='yellow'>          /     \\       </span>"
    },
    small = {
        "<span color='yellow'>     \\   /    </span>",
        "<span color='yellow'>      .-.     </span>",
        "<span color='yellow'>   ― (   ) ―  </span>",
        "<span color='yellow'>      `-’     </span>",
        "<span color='yellow'>     /   \\    </span>"
    },
}

local icon_fair_night = {
    big = {
        "<span color='white'>         _.._           </span>",
        "<span color='white'>       .' .-'`          </span>",
        "<span color='white'>      /  /              </span>",
        "<span color='white'>      ▏  ▏              </span>",
        "<span color='white'>      \\  \\___.;         </span>",
        "<span color='white'>       '._  _.'         </span>",
        "<span color='white'>          ``            </span>"
    },
    small = {
        "<span color='white'>              </span>",
        "<span color='white'>      .-.     </span>",
        "<span color='white'>     ( (      </span>",
        "<span color='white'>      `-’     </span>",
        "<span color='white'>              </span>"
    },

}

local icon_partly_cloudy = {
    big = {
        "<span color='yellow'>      \\   /             </span>",
        "<span color='yellow'>       .-.              </span>",
        "<span color='yellow'>   ―― (   </span><span color='grey'>.---.         </span>",
        "<span color='grey'>       .-(     )-.      </span>",
        "<span color='grey'>    .-(           )-.   </span>",
        "<span color='grey'>   (____._____)______)  </span>",
        "                        "
    },
    small = {
        "<span color='yellow'>    \\__/      </span>",
        "<span color='yellow'>  _ /  </span><span color='grey'>.-.    </span>",
        "<span color='yellow'>    \\_</span><span color='grey'>(   ).  </span>",
        "<span color='yellow'>    /</span><span color='grey'>(___(__) </span>",
        "              "
    }
}

local icon_partly_cloudy_night = {
    big = {
        "<span color='white'>       _.._             </span>",
        "<span color='white'>     .‘ .-‘             </span>",
        "<span color='white'>    /  /                </span>",
        "<span color='white'>    ▏  ▏</span><span color='grey'> .---.          </span>",
        "<span color='white'>    \\ </span><span color='grey'>.-(     )-.       </span>",
        "<span color='grey'>   .-(           )-.    </span>",
        "<span color='grey'>  (____._____)______)   </span>"
    },
    small = {
        "<span color='white'>    .-.       </span>",
        "<span color='white'>   ( (</span><span color='grey'>.-.     </span>",
        "<span color='white'>    ‘</span><span color='grey'>(   ).   </span>",
        "<span color='grey'>    (___(__)  </span>",
        "              ",
    }
}

local icon_fog = {
    big = {
        "                        ",
        "<span color='grey'>  _ - _ - _ - _ - _ - _ </span>",
        "<span color='grey'>   _ - _ - _ - _ - _ -  </span>",
        "<span color='grey'>  _ - _ - _ - _ - _ - _ </span>",
        "<span color='grey'>   _ - _ - _ - _ - _ -  </span>",
        "<span color='grey'>  _ - _ - _ - _ - _ - _ </span>",
        "                        "
    },
    small = {
        "              ",
        "<span color='grey'>  _ - _ - _ - </span>",
        "<span color='grey'>   _ - _ - _  </span>",
        "<span color='grey'>  _ - _ - _ - </span>",
        "              "
    }
}

local icon_heavy_rain = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "<span color='blue'>      ,‘,‘,‘,‘,‘,‘,‘    </span>",
        "<span color='blue'>    ,‘,‘,‘,‘,‘,‘,‘      </span>",
        "<span color='blue'>   ‘,‘,‘,‘,‘,‘,‘        </span>"
    },
    small = {
        "<span color='grey'>      .-.     </span>",
        "<span color='grey'>     (   ).   </span>",
        "<span color='grey'>    (___(__)  </span>",
        "<span color='blue'>   ‚‘‚‘‚‘‚‘   </span>",
        "<span color='blue'>   ‚’‚’‚’‚’   </span>"
    }
}

local icon_heavy_snow = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "<span color='white'>      * * * * * * *     </span>",
        "<span color='white'>     * * * * * * * *    </span>",
        "<span color='white'>      * * * * * * *     </span>"
    },
    small = {
        "<span color='grey'>      .-.     </span>",
        "<span color='grey'>     (   ).   </span>",
        "<span color='grey'>    (___(__)  </span>",
        "<span color='white'>    * * * *   </span>",
        "<span color='white'>   * * * *    </span>"
    }
}
local icon_light_snow = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "<span color='white'>      *   *   *   *     </span>",
        "<span color='white'>        *   *   *   *   </span>",
        "<span color='white'>      *   *   *   *     </span>"
    },
    small = {
        "<span color='grey'>      .-.     </span>",
        "<span color='grey'>     (   ).   </span>",
        "<span color='grey'>    (___(__)  </span>",
        "<span color='white'>   *  *  *    </span>",
        "<span color='white'>  *  *  *     </span>"
    }
}

local icon_storm = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "      <span color='blue'>‘</span><span color='yellow'>/__</span><span color='blue'>‘,‘,</span> <span color='yellow'>/__</span><span color='blue'>,‘</span>    ",
        "      <span color='blue'>,‘</span> <span color='yellow'>/</span> <span color='blue'>,‘,‘ ,</span><span color='yellow'>/</span> <span color='blue'>‘</span>    ",
        "       <span color='blue'>‘       ‘</span>        "
    },
    small = {
        "<span color='grey'>     .-.      </span>",
        "<span color='grey'>    (   ).    </span>",
        "<span color='grey'>   (___(__)   </span>",
        "<span color='blue'>    ‘</span><span color='yellow'>/_</span> <span color='blue'>‘     </span>",
        "<span color='blue'>   ‘</span><span color='yellow'>  /</span><span color='blue'> ‘   </span>"
    }
}

local icon_tornado = {
    big = {
        "     ===============    ",
        "       ===========      ",
        "           ========     ",
        "            =====       ",
        "           ====         ",
        "          ===           ",
        "         ==             "
    },
    small = {
        "   ========   ",
        "    =====     ",
        "      ====    ",
        "       ==     ",
        "      ==      "
    }
}

local icon_snow_rain = {
    big = {
        "<span color='grey'>            .--.        </span>",
        "<span color='grey'>         .-(    ).      </span>",
        "<span color='grey'>      .-(         ).    </span>",
        "<span color='grey'>     (_____.____)___)   </span>",
        "      <span color='blue'>‘</span> <span color='white'>*</span>   <span color='blue'>‘</span> <span color='white'>*</span>  <span color='blue'>‘  ‘</span>   ",
        "       <span color='blue'>‘</span>  <span color='white'>*</span> <span color='blue'>‘</span>   <span color='white'>*</span> <span color='blue'>‘</span>     ",
        "    <span color='blue'>‘</span> <span color='white'>*</span>  <span color='blue'>‘</span>  <span color='white'>*</span> <span color='blue'>‘  ‘</span>      "
    },
    small = {
        "<span color='grey'>     .-.      </span>",
        "<span color='grey'>    (   ).    </span>",
        "<span color='grey'>   (___(__)   </span>",
        "    <span color='blue'>‘</span> <span color='white'>*</span> <span color='blue'>‘</span>     ",
        "   <span color='white'>*</span> <span color='blue'>‘ ‘</span> <span color='white'>*</span>    "
    }
}

local icon_windy = {
    big = {
        "<span color='#22989a'>                        </span>",
        "<span color='#22989a'>           -.           </span>",
        "<span color='#22989a'>        _____)  -.      </span>",
        "<span color='#22989a'>      ____________)     </span>",
        "<span color='#22989a'>           ____         </span>",
        "<span color='#22989a'>               )        </span>",
        "<span color='#22989a'>             -‘         </span>"
    },
    small = {
        "<span color='#22989a'>         -.   </span>",
        "<span color='#22989a'>   ________)  </span>",
        "<span color='#22989a'>   ______     </span>",
        "<span color='#22989a'>         )    </span>",
        "<span color='#22989a'>       -‘     </span>"
    }
}

local icon_hot = {
    big = {
        "<span color='white'>           _            </span>",
        "<span color='white'>          | |</span>  <span color='red'>--- !</span>    ",
        "<span color='white'>          |</span><span color='red'>|</span><span color='white'>|</span>           ",
        "<span color='white'>          |</span><span color='red'>|</span><span color='white'>|</span>           ",
        "<span color='white'>          |</span><span color='red'>|</span><span color='white'>|</span>           ",
        "<span color='white'>         (</span> <span color='red'>O</span> <span color='white'>)</span>          ",
        "<span color='white'>          `-‘           </span>"
    },
    small = {
        "              ",
        "<span color='white'>      || </span><span color='red'>-- !</span> ",
        "<span color='white'>      ||      </span>",
        "<span color='white'>      ||      </span>",
        "<span color='white'>     (__)     </span>"
    }
}

local icon_cold = {
    big = {
        "<span color='white'>           _            </span>",
        "<span color='white'>          | |           </span>",
        "<span color='white'>          | |           </span>",
        "<span color='white'>          | |           </span>",
        "<span color='white'>          | |           </span>",
        "<span color='white'>         ( </span><span color='blue'>O</span><span color='white'> ) </span><span color='blue'>--- !</span>    ",
        "<span color='white'>          `-‘           </span>"
    },
    small = {
        "              ",
        "<span color='white'>      ||      </span>",
        "<span color='white'>      ||      </span>",
        "<span color='white'>      ||      </span>",
        "<span color='white'>     (__)<span color='blue'>-- !</span> "
    }
}

local function get_temp_color(num)
    local numcolor = ""
    if (num < -5) then
        numcolor="'#000059'"
    elseif (num < 1) then
        numcolor="'#22989a'"
    elseif (num < 10) then
        numcolor = "'#007700'"
    elseif (num < 20) then
        numcolor = awtk.theme.textutils.colors[4]
    elseif (num < 27) then
        numcolor = awtk.theme.textutils.colors[3]
    elseif (num < 35) then
        numcolor = awtk.theme.textutils.colors[2]
    else
        numcolor = awtk.theme.textutils.colors[1]
    end
    return numcolor
end

local function get_day(datum)
    local fd = io.popen("date -d '" .. datum .."' '+%a'")
    local status = fd:read("*all")
    return status:gsub("\n","")
end

local function create_forecast(forecast, unit)
    local retval = {}
    local str = string.format("%-18s ", forecast.day .. ", " .. forecast.date)
    table.insert(retval,"<span color='white'>" .. str .. "</span>")
    str = string.format("%9d",forecast.low)
    table.insert(retval,"LOW:" .. "<span color="..get_temp_color(forecast.low)..">" .. str .."</span> &#176;".. unit .. "   ")
    str = string.format("%8d",forecast.high)
    table.insert(retval,"HIGH:" .. "<span color="..get_temp_color(forecast.high)..">".. str .. "</span> &#176;" .. unit .. "   ")
    if (string.len(forecast.text) > 16) then
        str = string.sub(forecast.text,1,16) .. "   "
    else
        str = string.format("%-18s ",forecast.text)
    end
    table.insert(retval,str)
    table.insert(retval,"                   ")
    return retval
end

local function inarray(code,array)
    for _, k in pairs(array) do
        if k == code then
            return true
        end
    end
    return false
end

local function get_icon_by_code(code, isbig)
    local icon = {}
    if inarray(code, {389,386, 15, 16, 17, 41, 42}) then
        icon = icon_storm
    elseif inarray (code, {377,374,350,320,317, 19, 20, 21, 24, 25, 29, 43}) then
        icon = icon_snow_rain
    elseif inarray(code, {353,311,296,293,281,266,263,200,185,182,176, 12, 13, 14, 26, 39, 40}) then
        icon = icon_light_rain
    elseif inarray (code, {359,356,314,308,305,302,299,284, 18}) then
        icon = icon_heavy_rain
    elseif inarray(code, {392,368,365,362,326,323,227,179, 23, 44}) then
        icon = icon_light_snow
    elseif inarray(code, {395,371,338,335,332,329,230, 22}) then
        icon = icon_heavy_snow
    elseif inarray(code,{113, 1, 2, 3, 33, 34}) then
            icon = icon_sunny
    elseif (code == 5555) then
            icon = icon_fair_night
    elseif inarray(code, {143, 260, 248, 11}) then
        icon = icon_fog
    elseif inarray(code, {122, 119, 7, 8, 38}) then
        icon = icon_cloudy
    elseif inarray(code, {116, 4, 5, 6, 35, 36, 37}) then
            icon = icon_partly_cloudy
    elseif code == 2555 then
	    icon = icon_partly_cloudy_night
    elseif code == 30 then
	icon = icon_hot
    elseif code == 31 then
	icon = icon_cold
    elseif code == 32 then
	icon = icon_windy
    end
    if (isbig) then
        return icon.big
    end
    return icon.small
end

local function get_arrow(num)
    if (num > 337 or num < 22) then
        return "▲"
    elseif (num >21 and num < 67) then
        return "◥"
    elseif (num >66 and num < 112) then
        return "▶"
    elseif (num >111 and num < 147) then
        return "◢"
    elseif (num >146 and num < 202) then
        return "▼"
    elseif (num >201 and num < 247) then
        return "◣"
    elseif (num >246 and num < 292) then
        return "◀"
    elseif (num >291 and num < 338) then
        return "◤"
    end
end

function weatherutil.get_big_temp(self,arr)
    local numcolor = get_temp_color(self._temp)
    local clrstring = "<span color = "..numcolor..">"
    local fontclr = {}
    local fontclrend = {}
    local fontwhiteclr = {}
    for _,_ in pairs(font[1]) do
        table.insert(fontclr,clrstring)
        table.insert(fontclrend,"</span>")
        table.insert(fontwhiteclr,"<span color = 'white'>")
    end
    local str = string.format("%3d",self._temp)
    table.insert(arr,fontclr)
    for char in str:gmatch("(.)") do
        if (char == '-') then
            table.insert(arr,font[12])
        elseif (char == " ") then
            table.insert(arr,font[11])
        else
            table.insert(arr,font[string.byte(char)-47])
        end
    end
    table.insert(arr,fontclrend)
    table.insert(arr,fontwhiteclr)
    if (self._tempunit=="C") then
        table.insert(arr,celsius_unit)
    else
        table.insert(arr,farenheit_unit)
    end
    table.insert(arr,fontclrend)
end



function weatherutil.create_current_weather_table(self)
    local arr = {}
    local str = string.format("%21s", self._sunrise)
    local tmp =self._windspeed .. " " .. self._speedunit;
    local str2 = string.format("%19s",tmp)
    table.insert (arr, "SUNRISE: " .. str .. "   WINDSPEED: " .. str2)
    str = string.format("%22s", self._sunset)
    str2 = string.format("%16s", get_arrow(self._winddir))
    table.insert (arr, "SUNSET: " .. str .. "   WIND DIRECTION: " .. str2)
    str = string.format("%23s", self._humidity .. " %   ")
    str2 = string.format("%17s", self._text)
    table.insert (arr, "HUMIDITY: " .. str .. "DESCRIPTION: " .. str2)
    return arr
end

function weatherutil.get_strings(self)
    local arr = {}
    local main = {}
    --find forecast in case of bad data (bad forecast)
    local fr = self._forecasts[1]
    for i = 2,#self._forecasts do
        if fr.date == self._curdate then
            break
        end
        fr = self._forecasts[i]
    end
    if (fr ~= nil) then
        local tmp = string.format("%8d", fr.low)
        local colorlow = "<span color="..get_temp_color(fr.low)..">"..tmp.."</span>"
        tmp = string.format("%8d", fr.high)
        local colorhigh = "<span color="..get_temp_color(fr.high)..">"..tmp.."</span>"
        table.insert(arr,"                                                               ")
        local str = string.format(" %-26s",self._curday .. ", " .. self._curdate)
        table.insert(arr,"<span color='white'>"..self._city .. "</span>" .. str .. "LOW " .. colorlow .."&#176;".. self._tempunit .. "  MAX " .. colorhigh.."&#176;".. self._tempunit )
        table.insert(arr,"                                                               ")
        table.insert(main,get_icon_by_code(self._code,true))
        self:get_big_temp(main)
        tmp = awtk.textutils.combine_arrays(main)
        for _, k in pairs(tmp) do
            table.insert(arr,k)
        end
        table.insert(arr,"                                                               ")
        tmp = self:create_current_weather_table()
        for _, k in pairs(tmp) do
            table.insert(arr,k)
        end
        table.insert(arr,"                                                               ")
        local icon1 = get_icon_by_code(self._forecasts[2].code,false)
        local text1 = create_forecast(self._forecasts[2],self._tempunit)
        local icon2 = get_icon_by_code(self._forecasts[3].code,false)
        local text2 = create_forecast(self._forecasts[3],self._tempunit)
        tmp = awtk.textutils.combine_arrays({icon1,text1,icon2,text2})
        for _, k in pairs(tmp) do
            table.insert(arr,k)
        end
        table.insert(arr,"                                                               ")
        icon1 = get_icon_by_code(self._forecasts[4].code,false)
        text1 = create_forecast(self._forecasts[4],self._tempunit)
        icon2 = get_icon_by_code(self._forecasts[5].code,false)
        text2 = create_forecast(self._forecasts[5],self._tempunit)
        tmp = awtk.textutils.combine_arrays({icon1,text1,icon2,text2})
        for _, k in pairs(tmp) do
            table.insert(arr,k)
        end
        return arr
    end
end

function weatherutil.update(self)
	self._is_command_running = false
	local desklet_text = ""
	local arr = self:get_strings()
	if (arr ~= nil) then
		for i, k in pairs(arr) do
			desklet_text = desklet_text .. k
			if i < #arr then
				desklet_text = desklet_text .. "\n"
			end
		end
		if (self._desklet == nil) then
            local ws = screen[1].geometry
			self._desklet = awtk.desklet.new(desklet_text, {x=ws.x, y = ws.y + 1 + awtk.theme.titlebar.geometry.height})
			self._desklet:show()
		else
			self._desklet:update_text(desklet_text)
		end
	end
end

function weatherutil.set_error_status(self, status)
	self._error_text = "<span color='white'><b>Weather plugin:</b>\n" .. status .. "</span>\n\n" .. string.format("%" .. string.len(status) - 3 .."s","closing in ")
	self._counter = 5
	self._notify = awtk.notify.new("error",self._error_text .. self._counter .. "s")
	self._error_status=true
	self._notify:show()
end

function weatherutil.parse_current(self, data)
	if (data.Code == nil) then
		local year, month, day, _, _, _ = string.match(data.LocalObservationDateTime, "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)([%+%-])(%d+)%:(%d+)")
		self._curdate=day .. "." .. month .. " " .. year
		self._curday=get_day(self._curdate)
		self._temp=((metric == true) and data.Temperature.Metric.Value or data.Temperature.Imperial.Value)
		self._tempunit = ((metric == true) and data.Temperature.Metric.Unit or data.Temperature.Imperial.Unit)
		self._humidity=data.RelativeHumidity
		self._winddir=data.Wind.Direction.Degrees
		self._windspeed=((metric == true) and data.Wind.Speed.Metric.Value or data.Wind.Speed.Imperial.Value)
		self._speedunit = ((metric == true) and  data.Wind.Speed.Metric.Unit or data.Wind.Speed.Imperial.Unit)
		self._windchill = ((metric == true) and data.WindChillTemperature.Metric.Value or data.WindChillTemperature.Imperial.Value)
		self._code = data.WeatherIcon
		self._text = data.WeatherText
		self._has_current=true
	else
		self:set_error_status (data.Message)
	end
end

function weatherutil.parse_location(self, data)
	if (data.Code == nil) then
		self._location=data.Key
		self._city=data.LocalizedName
		print(self._location)
		self._has_location = true
	else
		self:set_error_status (data.Message)
	end
end

function weatherutil.parse_forecast(self, data)
	if (data.Code == nil) then
		self._forecasts = {}
		for daycount = 1, #data.DailyForecasts do
			local forecast={}
			local year, month, day, _, _, _ = string.match(data.DailyForecasts[daycount].Date, "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)([%+%-])(%d+)%:(%d+)")
			forecast.date = day .. "." .. month;
			forecast.day = get_day(year .. "-" .. month .. "-" .. day)
			forecast.low = data.DailyForecasts[daycount].Temperature.Minimum.Value
			forecast.high = data.DailyForecasts[daycount].Temperature.Maximum.Value
			forecast.text = data.DailyForecasts[daycount].Day.IconPhrase
			forecast.code = data.DailyForecasts[daycount].Day.Icon
			if (daycount == 1) then
				local _, _, _, hour, minute, second = string.match(data.DailyForecasts[daycount].Sun.Rise, "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)([%+%-])(%d+)%:(%d+)")
				self._sunrise = hour .. ":" .. minute .. ":" .. second
				_, _, _, hour, minute, second = string.match(data.DailyForecasts[daycount].Sun.Set, "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)([%+%-])(%d+)%:(%d+)")
				self._sunset = hour .. ":" .. minute .. ":" .. second
			end
			table.insert(self._forecasts,forecast)
		end
		self._has_forecast=true
	else
		self:set_error_status (data.Message)
	end
end

function weatherutil.get_current(self)
	print("get_current")
	if (self._wait_current==false) then
		self._wait_current=true
		local param = " --silent 'http://dataservice.accuweather.com/currentconditions/v1/" .. self._location .. "?apikey=".. accuweather_api_key .. "&details=true'"
		awful.spawn.easy_async (curlcmd .. param,
					function (stdout, _, _, _)
						local parsed = json.decode(stdout)
						weather_obj:parse_current(parsed[1])
					end)
	end
end

function weatherutil.get_location(self)
	print("get_location")
	if (self._wait_location==false) then
		self._wait_location=true
		local param = " --silent 'http://dataservice.accuweather.com/locations/v1/cities/ipaddress?apikey=".. accuweather_api_key .. "&q=" .. self._ip_address .. "'"
		print(curlcmd .. param)
		awful.spawn.easy_async (curlcmd .. param,
					function (stdout, _, _, _)
						local parsed = json.decode(stdout)
						weather_obj:parse_location(parsed)
					end)
	end
end

function weatherutil.get_forecast(self)
	print("get_forecast")
	if (self._wait_forecast==false) then
		self._wait_forecast=true
		local param = " --silent 'http://dataservice.accuweather.com/forecasts/v1/daily/5day/" .. self._location .. "?apikey=" .. accuweather_api_key .. "&details=true&metric=".. ((metric == true) and "true" or "false") .."'"
		awful.spawn.easy_async (curlcmd .. param,
					function (stdout, _, _, _)
						local parsed = json.decode(stdout)
						weather_obj:parse_forecast(parsed)
					end)
	end
end
function weatherutil.get_ip(self)
	if (self._wait_ip==false) then
		self._wait_ip=true
		local param = " --silent 'https://api.ipify.org'"
		awful.spawn.easy_async (curlcmd .. param,
					function (stdout, _, _, _)
						weather_obj._ip_address = string.gsub(stdout,"\n","")
						weather_obj._has_ip = true
					end)
	end
end

function weatherutil.exec_command(self)
	if (self._error_status) then
		if (self._counter > 0 ) then
			self._counter = self._counter - 1
			self._notify:update_text(self._error_text .. self._counter .. "s")
		else
			self._notify:hide()
			-- Try again in few hours
			self._counter=update_cycle
			self._has_current=false
			self._wait_current=false
			self._has_location=false
			self._wait_location=false
			self._has_forecast=false
			self._wait_forecast=false
			self._has_ip=false
			self._wait_ip=false
		end
	elseif (self._counter < 1) then
        if (location_by_ip == true) then
            if (self._has_ip == false) then
                self:get_ip()
            elseif (self._has_location == false) then
                self:get_location()
            end
        else
            self._has_location=true
            self._location=location
            self._city=city
        end
        if (self._has_current == false) then
            self:get_current()
        elseif (self._has_forecast == false) then
			self:get_forecast()
		else
			self:update()
			self._counter=update_cycle
			self._has_current=false
			self._wait_current=false
			self._has_location=false
			self._wait_location=false
			self._has_forecast=false
			self._wait_forecast=false
			self._has_ip=false
			self._wait_ip=false
		end
	else
		self._counter = self._counter - 1
	end
end

function weatherutil.show(self)
    self:exec_command()
    self._looperid = awtk.looper:register(function() weather_obj:exec_command() end)
    self._updatecycle = 0
end

function weatherutil.new()
    local self = setmetatable({}, weatherutil)
    self._city = ""
    self._location=""
    self._tempunit = ((metric == true) and "C" or "F")
    self._distunit = ((metric == true) and "m" or "M")
    self._pressureunit = ""
    self._speedunit = ""
    self._windchill = 0
    self._winddir = 0
    self._humidity = 0
    self._notify = nil
    self._windspeed = 0
    self._sunrise = ""
    self._sunset = ""
    self._current = ""
    self._temp = 0
    self._text = ""
    self._code = 0
    self._curday= ""
    self._curdate = ""
    self._has_ip = false
    self._ip_address = ""
    self._forecasts={}
    self._desklet = nil
    self._is_command_running=false
    self._looperid = nil
    self._wait_loc = false
    self._counter = 0
    self._latitude = 0
    self._longitude = 0
    self._has_current=false
    self._wait_current=false
    self._has_location=false
    self._wait_location=false
    self._has_forecast=false
    self._wait_forecast=false
    self._has_ip=false
    self._wait_ip=false
    self._error_status=false
    self._error_text=""
    return self
end

local function get_instance()
    if (weather_obj == nil) then
        weather_obj = weatherutil.new()
    end
    return weather_obj
end

return get_instance()
