-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local separator = require("separator")
local awtk = require("awtk")
local brightness = require("brightness")
local winlist = require("winlist")
local modelist = require("modelist")
local weather = require("weather")
local kernelinfo = require("kernelinfo")
local calendar = require("calendar")
local cpu = require("cpu")
local volume = require("volume")
local battery = require("battery")
local mainmenu = require("mainmenu")
local connman = require("connman")
require("awful.autofocus")
topbar = require("topbar")
local terminalclient = nil
function maximize(c) 
    c.maximized = false
    c.maximized=true
end

volume:setplayer("mpc")
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
                 end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("/home/spacok/.config/awesome/themes/black/theme.lua")
--theme.wallpaper = "/home/spacok/Images/Background/girllovegentoo-1920x1080.png"
--" .. tostring(screen[1].width) .. "x" .. tostring(screen[1].height) .. ".png" }

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor_cmd = ""

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
mainmenu:set_keys(modkey,'`')
--winlist:set_keys(modkey,'w')
modelist:set_keys(modkey,'d')

layouts = {
    {name = "FLT", icon = "panel/floating", layout = awful.layout.suit.floating},
    {name = "FRH", icon = "panel/fairh", layout = awful.layout.suit.fair.horizontal},
    {name = "FRV", icon = "panel/fairv", layout = awful.layout.suit.fair},
    {name = "TLB", icon = "panel/tilebottom", layout = awful.layout.suit.tile.bottom},
    {name = "TLT", icon = "panel/tiletop", layout = awful.layout.suit.tile.top},
    {name = "TLL", icon = "panel/tileleft", layout = awful.layout.suit.tile.left},
    {name = "TLR", icon = "panel/tileright", layout = awful.layout.suit.tile},
    {name = "MAX", icon = "panel/maximized", layout = awful.layout.suit.max},
}
tags = {
    {name = "WEB", category = "Internet", texticon = "󰖟", icon = "panel/web" , screen = 1, layout = 6},
    {name = "WORK", category = "Windows", texticon = "", icon = "panel/windows", screen = 2, layout = 6},
    {name = "DEV", category = "Development", texticon = "", icon = "panel/devel", screen = 2, layout = 5},
    {name = "GFX", category = "Graphics", texticon = "", icon = "panel/paint", layout = 1},
    {name = "ELEC", category = "Electronics", texticon ="", icon = "panel/circuit", layout = 1},
    {name = "MEDIA", category = "Multimedia", texticon = "󰟴", icon = "panel/television-classic", screen = 3, layout = 8},
    {name = "OTHER", category = "Other", texticon = "", icon = "panel/questionmark", layout = 4},
}
local tagmanager = awtk.tagmanager
tagmanager:set(tags, layouts)
tagmanager:set_modkey(modkey)
tagmanager:set_tagkey('t')
tagmanager:set_layoutkey('l')
myawesomemenu = {
  { "edit config", "apps/gnome-system-config", editor_cmd .. " " .. awesome.conffile },
  { "restart", "actions/gtk-refresh", awesome.restart },
  { "quit", "actions/application-exit", awesome.quit }
}

-- {{{ Wallpaper
for s in screen do
    -- Wallpaper for each screen
    gears.wallpaper.maximized("/home/spacok/Images/Background/girllovegentoo-" .. s.geometry.width .. "x" .. s.geometry.height .. ".png", screen[s])
    print(s.index)
    -- top bar for each screen

    local mywibox = awful.wibar({ position = "top",
                                  height = awtk.theme.menubar.height,
                                  screen = s.index})
                                    
    -- Widgets that are aligned to the left

    local left_layout
    local right_layout
    if s == screen.primary then
        left_layout = awtk.powerline.leftlayout({awtk.theme.menubar.color.bg2,
                                                 awtk.theme.menubar.color.bg}, 
                                                 awtk.theme.menubar.color.selected)
        right_layout = awtk.powerline.rightlayout({awtk.theme.menubar.color.bg2,
                                                   awtk.theme.menubar.color.bg}, 
                                                   awtk.theme.menubar.color.selected)
        left_layout:add(mainmenu:widget())
        left_layout:add(tagmanager:taglist(s.index))
        left_layout:add(tagmanager:layoutlist(s.index))
        right_layout:add(cpu:cpulayout())
        right_layout:add(cpu:memlayout())
        right_layout:add(volume:widget())
        right_layout:add(connman)
        right_layout:add(battery:widget())
        right_layout:add(calendar:datewidget())
        right_layout:add(calendar:clockwidget())
        beautiful.bg_systray = color1
        beautiful.systray_icon_spacing=awtk.theme.menubar.spacing + 3
        local tmplayout = wibox.layout.fixed.horizontal()
        tmplayout:add(wibox.widget.systray())
        tmplayout:add(modelist:get_widget())
        tmplayout:add(winlist)
        tmplayout.spacing = awtk.theme.menubar.spacing
        right_layout:add(tmplayout)
    else
        left_layout = awtk.powerline.leftlayout({awtk.theme.menubar.color.bg,
                                                 awtk.theme.menubar.color.bg2}, 
                                                 awtk.theme.menubar.color.selected)
        right_layout = awtk.powerline.rightlayout({awtk.theme.menubar.color.bg2,
                                                   awtk.theme.menubar.color.bg}, 
                                                   awtk.theme.menubar.color.selected)
        left_layout:add(tagmanager:taglist(s.index))
        left_layout:add(tagmanager:layoutlist(s.index))
        right_layout:add(calendar:datewidget())
        right_layout:add(calendar:clockwidget())
    end
    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout:layout())
    layout:set_middle(topbar:get_widget(s.index))
    layout:set_right(right_layout:layout())

    mywibox:set_widget(layout)
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu

mainmenu:awesome_menu(myawesomemenu)

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox
-- Create a wibox for each screen and add it
colors = {
    awtk.theme.menubar.color.bg2,
    awtk.theme.menubar.color.bg,
}
mywibox = {}
-- Create the wibox
for s in screen do
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 1, 
        function () 
            if mainmenu:visible()==true then 
                mainmenu:hide()
            end
        end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}
-- {{{ Key bindings
globalkeys = awful.util.table.join(
-- Multimedia keys for controling mpd
    awful.key({ modkey, "Control" }, "Up",
        function ()
            volume:player():toggle()
        end),
    awful.key({ modkey, "Control" }, "Down",
        function ()
            volume:player():pause()
        end),
    awful.key({ modkey, "Control" }, "Left",
        function ()
            volume:player():previous()
        end),
    awful.key({ modkey, "Control" }, "Right",
        function ()
            volume:player():next()
        end),
-- widgets keyboard controls
    awful.key({ modkey, "Control" }, "c",
        function() 
            cpu:notify_keyb_show() 
        end),
    awful.key({ modkey, "Control" }, "b",
        function() 
            battery:notify_keyb_show() 
        end),
    awful.key({ modkey, "Control" }, "v",
        function() 
            volume:notify_keyb_show() 
        end),
    awful.key({ modkey, "Shift" }, "v",
        function() 
            volume:player():run()
        end),
    awful.key({ modkey, "Control" }, "d",
        function() 
            calendar:show() 
        end),
    awful.key({ modkey, "Control" }, "n",
    	function()
	    connman_show()
	end),
   
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    -- Layout manipulation
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    awful.key({ modkey,           }, "Escape",
        function ()
			if (client.focus and client.focus.name == 'DropDown') then
                local tmp=awful.client.next(-1)
				client.focus.minimized = true
                if tmp then
                    tmp:raise()
                end
                topbar:update()
			else
    			local matcher = function (c)
        							return awful.rules.match(c, {name = 'DropDown'})
    							end
				screen_resolver = awful.util.getdir("config").."/scripts/screen.sh" 
    			awful.client.run_or_raise(terminal .. ' -t DropDown --class DropDown -e ' .. screen_resolver, matcher)
                topbar:update()
			end
        end),
    -- Standard controls
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),
    awful.key({ modkey,           }, "d", function () modelist:show() end),
    awful.key({ modkey,           }, "t", function () tagmanager:taglistshow() end),
    awful.key({ modkey,           }, "l", function () tagmanager:layoutlistshow() end),
    awful.key({ modkey, "Control" }, "n", awful.client.restore),
    -- Volume
    awful.key({ }, "XF86AudioRaiseVolume", function () volume:inc() end),
    awful.key({ }, "XF86AudioLowerVolume", function () volume:dec() end),
    awful.key({ }, "XF86AudioMute", function () volume:mute() end),
    awful.key({ modkey}, "a", function () volume:menu_show() end),
    awful.key({ }, "XF86MonBrightnessUp", function () brightness_inc(); brightness_set()  end),
    awful.key({ }, "XF86MonBrightnessDown", function () brightness_dec(); brightness_set() end),

    -- Prompt
    awful.key({ modkey },            "r", function () awful.spawn('rofi -show-icons -show') end),
    awful.key({ modkey },            "w", function () awful.spawn('rofi -show window -lines 15') end),
    awful.key({ modkey },            "y", function () awful.spawn(awful.util.getdir("config") .. "/scripts/ytfzf.sh") end),
    awful.key({ modkey }, "Return", function () awful.spawn(terminal) end),
    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey }, "`", function() mainmenu:show(modkey,'`') end)
    --awful.key({                   }, "#133", function () print ("Keypres") end, function() print("KeyRelease") end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey,           }, "q",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            if (not c.skip_taskbar) then
        	    c.minimized = true
    	    end
        end),
    awful.key({ modkey,           }, "m", function (c) topbar:maximize(c) end),
    awful.key({ modkey, "Shift"   }, "l", function () awful.spawn("/usr/bin/xtrlock -b -f") end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
    -- View tag only.
    awful.key({ modkey }, "F" .. i,
              function ()
                   tagmanager:activate_tag(tagmanager:gettag(i))
              end),
    -- Move client to tag.
    awful.key({ modkey, "Control" }, "F" .. i,
              function ()
                  if client.focus then
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if tag then
                          awful.client.movetotag(tag)
                          tagmanager:activate_tag(tagmanager:gettag(i))
                      end
                  end
              end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, function(c) if (not c.dockable) then awful.mouse.client.move(c) end end),
    awful.button({ modkey }, 3, function(c) if (not c.dockable) then awful.mouse.client.resize(c) end end))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	{ rule = { name = "DropDown" },
	  properties = {ontop = true,
                    floating = true,
                    maximized_horizontal = true,
                    maximized_vertical = true,
                    raise = true,
                    maximized = true,
                    sticky = true}
	},
    { rule = { class = "Xephyr" },
      properties = {floating = false,
                    maximized_horizontal = false,
                    maximized_vertical = false,
                    maximized = false}

    },
    {
        rule = {class = "connman"},
        properties = {floating = true,
                    maximized_horizontal = false,
                    maximized_vertical = false,
                    maximized = false}
    },
    { rule = {type = "dialog" },
	  properties = {ontop = true,
                    floating = true,
                    maximized_horizontal = false,
                    maximized_vertical = false,
                    raise = true,
                    maximized = false,
                    sticky = false}
    },
	{ rule = { class = "Dragon" },
	  properties = {sticky = true,
                    ontop = true,
                    floating = true}
	},
    { rule = { class ="mpv" },
	  properties = {sticky = false,
                    ontop = true,
                    floating = true},
      callback = function(c) topbar:get_titlebar(c) end                     
    },
    { rule = { role = "browser"},
      properties = {floating = false,
                    maximized_horizontal = true,
                    maximized_vertical = true,
                    maximized = true}
    },
    { rule = { class = "rdesktop" },
      properties = {floating = false,
                    maximized_horizontal = true,
                    maximized_vertical = true,
                    maximized = true}
    },
    { rule = { role = "pop-up" },
      properties = {floating = false,
                    maximized_horizontal = false,
                    maximized_vertical = false,
                    maximized = false}
    },

	{ rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     maximized_horizontal = false,
                     maximized_vertical = false,
                     maximized = false,
                     raise = false,
                     buttons = clientbuttons,
                     keys = clientkeys,
                     maximized = false}
    },
	
}
-- }}}

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
    c:move_to_screen(awful.screen.focused())
    topbar:get_titlebar(c)
end)
awful.screen.connect_for_each_screen(function(s) 
                                        topbar:update()
                                    end)
-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)


weather:show()
kernelinfo:show()

