local modelist = {}
local modelist_obj = nil
local awtk = require("awtk")
local wibox = require("wibox")
local awful = require ("awful")
local tags = require("awful.tag")
modelist.__index = modelist


local outputs = {}
local curOutput = nil
local mainoutput = ""

local function xrandr_isOutput(str)
    local output = {}
    if string.find(str,"connected") ~= nil then
        output['name']=str:match("^.- ")
        output['modes']={}
        if string.find(str,"disconnected") ~= nil then
            output['connected']=false
        else
            output['connected']=true
        end
        if curOutput==nil then
            mainoutput=output.name
        end
        curOutput=output 
        table.insert(outputs,curOutput)
        return true
    end
    return false
end

local function xrand_isMode(str)
    if (curOutput ~= nil) then
        local mode = {}
        mode['resolution']=string.match(str,"[^".." ".."]+")
        if string.find(str,'*+') ~= nil then
            mode['isactive']=true
        else
            mode['isactive']=false
        end
        table.insert(curOutput.modes,mode)
    end
end

local function xrandr_parse()
    local fd = io.popen("xrandr -q")
    local status = fd:read("*line")
    curOutput = nil
    while (status ~= nil) do
        if (xrandr_isOutput(status) ~= true) then
            xrand_isMode(status)
        end
        status=fd:read("*line")
    end
    fd:close()
end

function modelist.close(self)
    if (self._menu) then
        self._menu:hide()
    end
end

function modelist.generate(self)
    xrandr_parse()
    local outputs_t = {}
    for k, c in pairs(outputs) do
        local name = ""
        if (str.len(c.name) > 25) then
            name =  string.sub(c.name,1,23)
            name = name .. '...'
        else
            name = c.name
        end
        if c.connected == true then
            local menus = {}
            local hasactive = false
            for j,m in pairs(c.modes) do
                local icon = "actions/unchecked"
                if m.isactive == true then
                    icon = "actions/checked"
                    hasactive = true
                end
                table.insert(menus,{m.resolution,icon,function ()
                                                        local cmd =" --output ".. c.name .. " --mode " .. m.resolution
                                                        if c.name ~= mainoutput then
                                                            cmd = " --output " .. mainoutput .."--primary" .. cmd .. " --right-of " .. mainoutput
                                                        end
                                                        cmd = "xrandr" .. cmd 
                                                        awful.util.spawn(cmd)
                                                        --awful.util.spawn(cmd) 
                                                      end})
            end
            if (hasactive == true) then
                off_icon = "actions/button_cancel"
                menu_icon = "devices/display-connected"
            else
                off_icon = "actions/button_ok"
                menu_icon = "devices/display-disconnected"
            end
            table.insert(menus,{"off",off_icon,function () 
                                                 awful.util.spawn("xrandr --output ".. c.name .. " --off") 
                                             end})
            table.insert(outputs_t,{name,menu_icon,menus})
        end
    end
    outputs={}
    return outputs_t
end

function modelist.show(self)
    if (not self._menu or self._menu:is_visible()==false) then 
        local menudata = modelist.generate(self)
        local ws = screen[1].geometry
        local menu_props = {x=ws.x + ws.width,
                            y=ws.y + 20,
                            modkey=self._modkey,
                            key=self._key,
                            enable_submenu = true}
        self._menu = awtk.menu.new(menudata,menu_props)
        self._menu:show()
    else
        self._menu:hide()
    end
end

function modelist.new()
    local self = setmetatable({}, modelist)
    self._modkey = nil
    self._key=nil
    self._menu=nil
    self._widget = wibox.layout.fixed.horizontal()
    self._iconbox = wibox.widget.imagebox()
    self._iconbox:set_image(awtk.theme.get_icon(24,"devices/display"))
    self._widget:add(self._iconbox)
    self._widget:buttons(awful.util.table.join(
        awful.button({ }, 1, function () modelist.show(self) end)
    ))
    for i=1,screen:count() do
        screen[i].index=screen:count()-i+1
    end
    return self
end
function modelist.set_keys(self,modkey,key)
    self._modkey=modkey
    self._key=key
end

function modelist.get_widget(self)
    return (self._widget)
end

local function get_instance()
    if (modelist_obj == nil) then
        modelist_obj = modelist.new()
    end
    return modelist_obj
end

return get_instance()
