local awtk = require("awtk")
local awful = require("awful")
local brightness = {}
local max_brightness = 0
local brightness_step = 10
local current_brightness = 0
local brightness_control = nil
local ws = screen[1].workarea
local counter=0

local geometry = {
    width = 155,
    height = 30,
    x = ws.width - 170,
    y = 35 ,
    vertical = false
}
local brightness_data ="/sys/class/backlight/amdgpu_bl0/"
local brightness_set_cmd = "${HOME}/.local/bin/sudo " .. awful.util.getdir("config") .. "scripts/backlight"

local function read_max_brightness()
    local file = io.open(brightness_data .. "max_brightness")
    max_brightness = tonumber(file:read("*line"))
end

local function read_current_brightness()
    local file = io.open(brightness_data .. "actual_brightness")
    current_brightness = tonumber(file:read("*line"))
end

local function read_data()
    if (max_brightness == 0) then
        read_max_brightness()
    end
    read_current_brightness()
end

function brightness_set()
    local cmd = brightness_set_cmd .. ' ' .. current_brightness
    print(cmd)
    awful.util.spawn_with_shell(cmd)
end

local function handle_progressbar(button, percentage)
    if (button == 1) then
        current_brightness = math.floor(max_brightness * (percentage/100))
        brightness_control:set_value((current_brightness/max_brightness)*100)
    elseif (button == 4) then
        brightness_inc()
    elseif (button == 5) then
        brightness_dec()
    end
    print("Set cmd")
    brightness_set()
end

function brightness.set_datadir(datadir)
    brightness_data=datadir
end

local function create_widget()
    brightness_control = awtk.controlbar.new("brightness",geometry)
    brightness_control:set_callbacks(handle_progressbar,nil)
    brightness_control:set_value((current_brightness/max_brightness)*100)
end

function brightness_show()
    counter = 0;
    read_data()
    if (brightness_control == nil) then
        create_widget()
    end
    if (brightness_control:is_visible() == false) then
        brightness_control:show()
    end
end

function brightness_hide()
    brightness_control:hide()
end

function counter_brightness()
    counter = counter + 1
    if counter == 2 then
        if (brightness_control) then 
            brightness_control:hide()
        end
        counter = 0
    end
end

function brightness_inc()
    if (brightness_control == nil) then
        read_data()
        create_widget()
    end
    if (current_brightness < max_brightness) then
        counter = 0
        current_brightness = current_brightness + brightness_step
        if (current_brightness > max_brightness) then
            current_brightness = max_brightness
        end
    end
    brightness_control:set_value((current_brightness/max_brightness)*100)
    if (brightness_control:is_visible() == false) then
        brightness_control:show()
    end
end

function brightness_dec()
    if (brightness_control == nil) then
        read_data()
        create_widget()
    end
    if (current_brightness > 0) then
        counter = 0
        current_brightness = current_brightness - brightness_step
        if (current_brightness < 0) then
            current_brightness = 0
        end
    end
    brightness_control:set_value((current_brightness/max_brightness)*100)
    if (brightness_control:is_visible() == false) then
        brightness_control:show()
    end

end
awtk.looper:register(function() counter_brightness() end)

return brightness
