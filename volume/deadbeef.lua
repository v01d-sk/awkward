
local awtk = require("awtk")
local textutils = awtk.textutils
local awful = require("awful")


local dpid_cmd = "ps aux | awk '/[0-9] deadbeef/ {print $2}'"
local status_cmd = 'deadbeef --nowplaying-tf "%track% | %title% | %artist% | %year% | %album% | %playback_time_seconds% | %length_seconds% | %playback_time% | %length% | %ispaused% | %isplaying% | end"'
local deadbeef = {}


deadbeef.__index = deadbeef

local function errormsg(status)
    local ret_text
    ret_text = "<span color='white'><b>                    DEADBEEF INFORMATIONS:</b></span>\n"
    ret_text = ret_text .. "------------------------------------------------------------\n"
    ret_text = ret_text .. "\n Author: \t\t\t"
    ret_text = ret_text .. "Album:\n"
    ret_text = ret_text .. " Status: " .. status .. "\n"
    ret_text = ret_text .. " 0:00 ".. textutils.text_progressbar(0,41) .. ' -:--   0%'
    return ret_text
end

local function checkpid()
    local fd = io.popen(dpid_cmd);
    local line = fd:read("*line")
    fd:close()
    if line ~= nil then
        line = line:gsub("%s+","")
        return tonumber(line)
    end
    return -1
end

function deadbeef.status()
    if (checkpid() > 0) then
        local fd = io.popen(status_cmd)
        local tmpstr = fd:read("*all")
        fd:close()
        local t = {}
        for str in string.gmatch(tmpstr, "([^|]+)") do
            table.insert(t, str)
        end
        if (#t ~= 12) then
            return errormsg("Error in parsing output in parsing output. " .. #t)
        end
        local ret_text
        ret_text = "<span color='white'><b>                    DEADBEEF INFORMATIONS:</b></span>\n"
        ret_text = ret_text .. "------------------------------------------------------------\n"
        ret_text = ret_text .. " <b>" .. t[1].. " - " .. t[2] .. "</b>\n"
        local author = t[3]
        if (string.len(author) > 22) then
            author = string.sub(author,1,19)
            author = author .. "..."
        elseif (string.len(author) < 22) then
            local format = '%'..string.format(' %ds',22-string.len(author))
            author = author .. string.format(format," ")
        end
        ret_text = ret_text .." Author: " .. author
        local album = ""
        if t[4] ~= " " then
            album = "[" .. t[4] .. "] " .. t[5]
            if (string.len(album) > 21) then
                album = string.sub(album,1,18)
                album = album .. "..."
            end
        end
        ret_text = ret_text .. " Album: " .. album .. "\n"
        local strstatus = "stopped"
        if (t[10] == " 1 ") then
            strstatus = "paused"
        elseif (t[11] == " 1 ") then
            strstatus = "playing"
        end
        ret_text = ret_text .. " Status: " .. strstatus ..'\n'
        if t[6] ~= "  " and t[7] ~= "  " then
            local perc = math.floor((tonumber(t[6])/tonumber(t[7])) * 100)
            ret_text = ret_text .. ' ' .. t[8] .. ' ' .. textutils.text_progressbar(perc,40) .. ' ' .. t[9]  .. ' ' .. perc .. '%'
        else
            ret_text = ret_text .. " 0:00 ".. textutils.text_progressbar(0,41) .. ' -:--   0%'
        end
        return ret_text
    else
        return errormsg("DeaDBeeF not running")
    end
    return errormsg("Error")
end

function deadbeef.toggle()
    if checkpid() > 0 then
        awful.util.spawn_with_shell("deadbeef --random")
    end
end

function deadbeef.start()
    if checkpid() > 0 then
        awful.util.spawn_with_shell("deadbeef --start")
    end
end

function deadbeef.stop()

    if checkpid() > 0 then
        awful.util.spawn_with_shell("deadbeef --stop")
    end
end

function deadbeef.pause()
    if checkpid() > 0 then
        awful.util.spawn_with_shell("deadbeef --pause")
    end
end

function deadbeef.next()
    if checkpid() > 0 then
        awful.util.spawn_with_shell("deadbeef --next")
    end
end

function deadbeef.previous()
    if checkpid() > 0 then
        awful.util.spawn_with_shell("deadbeef --prev")
    end
end

function deadbeef.run()
    local cls = client.get()
    for _, c in pairs(cls) do
        if (string.lower(c.class) == "deadbeef") then
            if c:isvisible() then
                c.minimized = true
            else
                c.minimized = false
                client.focus = c
                c:raise()
            end
            return
        end
    end
    local matcher = function (c)
                        return awful.rules.match(c, {class = 'deadbeef'})
                    end
    awful.client.run_or_raise('deadbeef', matcher)
end

function deadbeef.new()
    local self = setmetatable({},deadbeef)
    return self
end

return deadbeef.new()
