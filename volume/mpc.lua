local awtk = require("awtk")
local textutils = awtk.textutils
local awful = require("awful")

local mpc = {}
mpc.__index = mpc

local mpc_status_cmd = 'mpc -f "%track% - %title%\n%artist%\n%album%\n" status'
local mpc_player = 'glurp'

local function mpc_error(status)
    local ret_text
    ret_text = "\n Author: \t\t\t"
    ret_text = ret_text .. "Album:\n"
    ret_text = ret_text .. " Status: " .. status .. "\n"
    ret_text = ret_text .. " 0:00 ".. textutils.text_progressbar(0,41) .. ' -:--   0%' 
    return ret_text
end

function mpc.status(self)
    local fd = io.popen(mpc_status_cmd)
    line = fd:read("*line")
    local ret_text
    ret_text = "<span color='white'><b>                      MPD INFORMATIONS:</b></span>\n"
    ret_text = ret_text .. "------------------------------------------------------------\n" 
    if line == nil then
	    return mpc_error("MPD Not running")
    end
    if line:match("%a+")=="volume" then 
	    return mpc_error("stopped")
    end
    ret_text = " <b>" .. line .. "</b>\n"
    local author = fd:read("*line")
    if (string.len(author) > 22) then
        author = string.sub(author,1,19)
        author = author .. "..."
    elseif (string.len(author) < 22) then
        local format = '%'..string.format(' %ds',22-string.len(author))
        author = author .. string.format(format," ")
    end
    ret_text = ret_text .." Author: " .. author .. ""
    local album = fd:read("*line")
    if (string.len(album) > 21) then
        album = string.sub(album,1,18)
        album = album .. "..."
    end
    ret_text = ret_text .. " Album: " .. album .. "\n"
    fd:read("*line")
    local line=fd:read("*line")
    
    local words = {}
    line:gsub('[^ ]+',function(i) table.insert(words,i) end)
    local strstatus = string.sub(words[1],2,-2)
    local times = {}
    words[3]:gsub('[^/]+',function(i) table.insert(times,i) end)
    local perc = string.sub(words[4],2,-3)
    if (perc ~= "100") then
        perc = string.format("% 3s",perc)
    end
    ret_text = ret_text .. " Status: " .. strstatus ..'\n'
    ret_text = ret_text .. ' ' .. times[1] .. ' ' .. textutils.text_progressbar(tonumber(perc),40) .. ' ' .. times[2]  .. ' ' .. perc .. '%' 
    fd:close()
    return ret_text 

end

function mpc.pause(self)
    awful.util.spawn_with_shell("mpc pause")
end

function mpc.previous(self)
    awful.util.spawn_with_shell("mpc previous")
end

function mpc.previous(self)
    awful.util.spawn_with_shell("mpc next")
end

function mpc.toggle(self)
    awful.util.spawn_with_shell("mpc toggle")
end

function mpc.run(self)
    local cls = client.get()
    local cls_t = {}
    for k, c in pairs(cls) do
        if (string.lower(c.class) == "glurp") then
            if (c.minimized == true) then
                c.minimized = false
            else
                c.minimized = true
            end
            return
        end
    end
    local matcher = function (c)
                        return awful.rules.match(c, {class = 'glurp'})
                    end
    awful.client.run_or_raise(mpc_client, matcher)
    awful.util.spawn_with_shell(mpc_player)
end

function mpc.new()
    local self = setmetatable({},mpc)
    return self
end
return mpc.new()
