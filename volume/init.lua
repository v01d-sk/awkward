local wibox = require("wibox")
local awful = require("awful")
local awtk = require("awtk")
local notify = awtk.notify
local textutils = awtk.textutils
local volume_obj=nil

local volume = {}
volume.__index = volume

function volume.notify_hide(self)
    if self._notification ~= nil then
        self._notification:hide()
    end
end

function volume.generate_notify_text(self)
    local strvolume = tostring(self._volume)
    local fd = io.popen("pacmd list-sinks | awk '/index:/{i++} /* index:/{print i; exit}'")
    local status = fd:read("*line")
    fd:close()
    local active = tonumber(status)
    fd = io.popen("pacmd list-sinks | grep alsa.card_name | cut -f2 -d'\"'")
    status = fd:read("*line")
    local counter = 1
    local device = ""
    while (status ~= nil) do
        if counter == active then
            device = status
            break
        end
        counter = counter + 1
        status=fd:read("*line")
    end
    fd:close()
    if (self._volume < 100) then
        strvolume = string.format("% 3d",self._volume)
    end
    local draw_text = '------------------------------------------------------------\n'
    draw_text = draw_text .. "<span color='white'><b>                     AUDIO INFORMATIONS:</b></span>\n"
    draw_text = draw_text .. "------------------------------------------------------------\n"
    draw_text = draw_text .. " Device: " .. device .. "\n"
    draw_text = draw_text .. " Master: " .. textutils.text_progressbar(self._volume,41) .. ' ' .. strvolume .. ' %\n'
    draw_text = draw_text .. " Status: " .. self._status .. '\n\n'
    draw_text = draw_text .. "------------------------------------------------------------\n"
    draw_text = draw_text .. self._player:status()
    return draw_text
end

function volume.update(self,update_counters)
    local fd = io.popen("amixer sget Master; exit")
    local status = fd:read("*all")
    local text=""
    fd:close()
    self._volume = tonumber(string.match(status, "(%d?%d?%d)%%"))
    if (self._volume == nil) then
	    self._volume = 0
    end
    self._status = string.match(status, "%[(o[^%]]*)%]")
    self._last_icon_name = self._icon_name
    if self._status ~= nil then
        if string.find(self._status, "on", 1, true) then
            -- For the volume numbers
            if (self._volume == 0) then
                self._icon_name = "panel/audio-off"
            elseif (self._volume < 33) then
                self._icon_name = "panel/audio-volume-low"
            elseif (self._volume < 66) then
                self._icon_name = "panel/audio-volume-medium"
            else
                self._icon_name = "panel/audio-volume-high"
            end
            self._status="On"
            text = awtk.textutils.colorize_number(100, 0, self._volume, false) .. "%"
        else
            -- For the mute buttons
            text = awtk.textutils.colorize_number(100, 0, 0, false) .. "%"
            self._icon_name = "panel/audio-volume-mute"
            self._status = "Mute"
        end
    else
        self._status="No Control"
        text = awtk.textutils.colorize_number(100, 0, 75, true) .. "%"
        self._icon_name = "panel/audio-volume-strong"
    end
    self._textbox:set_markup(text)
    if (self._last_icon_name ~= self._icon_name) then
        self._icon:set_image(awtk.theme.get_icon(24,self._icon_name))
    end
    if (self._notification and self._notification:isVisible()) then
        local draw_text = self:generate_notify_text()
        self._notification:update_text(draw_text)
    end
    if (self._control) then
        if (update_counters==true) then
            self._control_timeout = self._control_timeout + 1
        end
        self._control:set_value(self._volume)
        self._control:set_icon(self._icon_name)
    end
    if (self._control_timeout == 2) then
        self._control:hide()
        self._control_timeout=0
    end
end

function volume.notify_show(self)
    if (self._control == nil or self._control:is_visible()==false) then
        local draw_text = self:generate_notify_text()
        if (self._notification == nil) then
            self._notification = notify.new("devices/audio-speakers",draw_text)
        else
            self:update(true)
        end
        if (self._notification:isVisible() == false) then
            self._notification:show(screen.primary.index)
        end
    end
end

function volume.attach(self)
    self._widget:connect_signal("mouse::enter", function () volume_obj:notify_show() end)
    self._widget:connect_signal("mouse::leave", function () volume_obj:notify_hide() end)
end

function volume.notify_keyb_show(self)
    if (self._control and self._control:is_visible()) then
        self._control:hide()
    end
    if self._notification and self._notification:isVisible() then
        self:notify_hide()
    else
        self:notify_show()
    end
end

function volume.progressbar_value(self)
    local ws = screen[1].workarea
    local coords = mouse.coords()
    local value = coords.x-(ws.width - 120)
    local cmd = "amixer set Master "..value.."%"
    awful.util.spawn_with_shell(cmd)
    self._progressbar_timeout=0
    self:update(false)
end

local function handle_progressbar(button,percentage)
    if (button == 1) then
        volume_obj._control:set_value(percentage)
        local cmd = "amixer set Master "..percentage.."%"
        awful.util.spawn_with_shell(cmd)
        volume_obj._control_timeout=0
        volume_obj:update(false)
    elseif (button == 4) then
        volume_obj:inc()
    elseif (button == 5) then
        volume_obj:dec()
    end
end

local function handle_icon(button)
    if (button == 1) then
        volume_obj:mute()
    end
end

function volume.control_show(self)
    if (self._notification and self._notification:isVisible()) then
        self._notification:hide()
    end
    if self._control == nil then
        self._control = awtk.controlbar.new(self._icon_name,self.geometry)
        self._control:set_callbacks(handle_progressbar,handle_icon)
    end
    self._control_timeout=0
    self._control:show()
end

function volume.inc(self)
    if (self._notification==nil or self._notification:isVisible()==false) then
        if (self._control == nil or self._control:is_visible()==false) then
            self:control_show()
        end
    end
    self._control_timeout=0
    awful.spawn("amixer set Master 5%+")
    self:update(false)
end

function volume.dec(self)
    if (self._notification==nil or self._notification:isVisible()==false) then
        if (self._control == nil or self._control:is_visible()==false) then
            self:control_show()
        end
    end
    self._control_timeout=0
    awful.spawn("amixer set Master 5%-")
    self:update(false)
end

function volume.mute(self)
    self._control_timeout=0
    awful.spawn("amixer sset Master toggle")
    self:update(false)
end


function volume.control_toggle(self)
    if self._control and self._control:is_visible() then
        self._control:hide()
    else
        self:control_show()
    end
end

function volume.control_runapp(self)
    if (self._player ~= nil) then
        self._player:run()
    end
end

function volume.widget(self)
	return self._widget
end

function volume.player(self)
    return self._player
end

function volume.setplayer(self, player)
    self._player = require("volume." .. player)
end

local function get_sinks()
    local menu = {}
    local names = {}
    local sinks = {}
    local fd = io.popen("pacmd list-sinks | awk '/index:/{i++} /* index:/{print i; exit}'")
    local status = fd:read("*line")
    fd:close()
    local active = tonumber(status)
    fd = io.popen("pacmd list-sinks | grep name: | cut -f2 -d'<'| cut -f1 -d'>'")
    status = fd:read("*line")
    while (status ~= nil) do
        table.insert(sinks,status)
        status=fd:read("*line")
    end
    fd:close()
    fd = io.popen("pacmd list-sinks | grep alsa.card_name | cut -f2 -d'\"'")
    status = fd:read("*line")
    while (status ~= nil) do
        table.insert(names,status)
        status=fd:read("*line")
    end
    fd:close()
    for v, k in ipairs(sinks) do
        local icon = "actions/unchecked"
        if active == v then
            icon = "actions/checked"
        end
        table.insert(menu, {names[v],icon, function()
                                                 awful.util.spawn_with_shell("pactl set-default-sink " .. k)
                                                 volume_obj:menu_show()
                                             end})
    end
    return menu
end
function volume.menu_show(self)
    if (not self._menu or self._menu:is_visible()==false) then
        local menudata = get_sinks()
        local ws = screen[1].geometry
        local menu_props = {x=ws.x + ws.width,
                            y=ws.y + 20,
                            modkey=self._modkey,
                            key=self._key,
                            enable_submenu = true}
        self._menu = awtk.menu.new(menudata,menu_props)
        self._menu:show()
    else
        self._menu:hide()
    end
end

function volume.new()
    local self = setmetatable({},volume)
	self._notification = nil
    self._player = nil
    self._control = nil
	self._control_timeout = 0
	self._volume = 0
	self._icon_name = ""
	self._status = ""
	self._last_icon_name = ""
	self._widget = wibox.layout.fixed.horizontal()
	self._icon=wibox.widget.imagebox()
	self._widget:add(self._icon)
    self._menu = nil
    self._textbox = wibox.widget
    {
        markup="",
        align="center",
        valign="center",
        font = awtk.theme.menubar.font.family,
        widget=wibox.widget.textbox,
        forced_width = (awtk.theme.menubar.font.width + 1) * 4,
    }
    self._widget.spacing = awtk.theme.menubar.spacing
    self._widget:add(self._textbox)
	local ws = screen[1].workarea
	self.geometry = {
    	width = 155,
    	height = 30,
    	x = ws.width - 170,
    	y = 35 ,
    	vertical = false
	}
	awtk.looper:register(function() volume_obj:update(true) end)
	self._widget:buttons(awful.util.table.join(
    	awful.button({ }, 3, function () volume_obj:menu_show() end),
    	awful.button({ }, 1, function () volume_obj:control_runapp() end),
    	awful.button({ }, 4, function () volume_obj:inc() end),
    	awful.button({ }, 5, function () volume_obj:dec() end)
	))
	self:attach()
    return self
end
volume_obj = volume.new()
return volume_obj

