local winlist = {}
local winlist_obj = nil
local awtk = require("awtk")
local wibox = require("wibox")
local awful = require ("awful")
local tags = require("awful.tag")
winlist.__index = winlist

function winlist.generate_windowlist(self)
    local cls = client.get()
    local cls_t = {}
    for _, c in pairs(cls) do
        if (not c.skip_taskbar) then
        local name = ""
        if (c.name and string.len(c.name) > 48) then
            name =  string.sub(c.name,1,45)
            name = name .. '...'
        else
            if (c.name) then
                name = c.name
            else
                name = c.class
            end
        end
        local icon = string.lower(c.class)
        if (c.icon_name ~= nil and c.icon_name ~= "") then
            local icon_name = string.lower(c.icon_name)
            if (awtk.theme.check_icon(24,icon_name)==true) then
                icon = icon_name
            end
	    elseif (c.class:find("chromium")) then
            icon="chromium-browser"
        elseif (c.class:find("RSS")) then
            icon="rssguard"
        end
        print (icon)
        table.insert(cls_t,{name,
                        'apps/'..icon,
                        function ()
                            if not c:isvisible() then
                                tags.viewmore(c:tags(), c.screen)
                            end
                            client.focus = c
                            c:raise()
                            winlist.close(self)
                        end})
        end
    end
    if (#cls_t==0) then
        table.insert(cls_t,{"No window","empty",function() winlist.close(self) end})
    end
    return cls_t
end

function winlist.close(self)
    print ("Winlist::Close()")
    if (self._menu) then
        self._menu:hide()
    end
end

--function winlist.get_widget(self)
--    return (self._widget)
--end

function winlist.show(self)
    if (not self._menu or self._menu:is_visible()==false) then
        local menudata = winlist.generate_windowlist(self)
        local ws = screen[1].geometry
        local menu_props = {x=ws.x + ws.width,
                            y=ws.y + 20,
                            modkey=self._modkey,
                            key=self._key,
                            enable_submenu = false,
                            mouse_close=true}
        self._menu = awtk.menu.new(menudata,menu_props)
        self._menu:show()
    else
        self._menu:hide()
    end
end

function winlist.set_keys(self,modkey,key)
    self._modkey=modkey
    self._key=key
end

function winlist.new()
    local self = wibox.layout.fixed.horizontal()
    self._modkey = nil
    self._key=nil
    self._menu=nil
    self._widget = wibox.layout.fixed.horizontal()
    self._iconbox = wibox.widget.imagebox()
    self._iconbox:set_image(awtk.theme.get_icon(24,"apps/window_list"))
    self:add(self._iconbox)
    self:buttons(awful.util.table.join(
        awful.button({ }, 1, function () winlist.show(self) end)
    ))
    return self
end

local function get_instance()
    if (winlist_obj == nil) then
        winlist_obj = winlist.new()
    end
    return winlist_obj
end

return get_instance()
