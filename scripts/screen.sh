#!/bin/bash
export TERM=screen-256color
export SESSIONNAME="DropDown"
tmux -u attach -t ${SESSIONNAME} || tmux -u new -t ${SESSIONNAME}
