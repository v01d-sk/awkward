local textutils={}
local theme

function textutils.align(num, len)
    local format = '%'..string.format(" %dd",len)
    local txtnum = string.format(format,num)
    return txtnum
end

function textutils.align_string(str, len)
    local format = '%' .. string.format("%ds",len)
    return string.format(format, str)
end

function textutils.align_string_left(str, len)
    local format = '%' .. string.format("%ds",len)
    local tmp = string.format(format, " ")
    local tmp2 = string.sub(tmp,1, (-1 * string.len(str)))
    return str .. tmp2
end

function textutils.align_string_center(str, len)
    if (string.len(str) == len) then
        return str
    end
    local format = '%' .. string.format("%ds",len)
    local tmp = string.format(format, " ")
    local align = math.fmod(string.len(str),2)
    local first = string.sub(tmp, 1, math.floor((len / 2) - (string.len(str) / 2)))
    local second = string.sub(tmp, 1, math.floor((len / 2) - (string.len(str) / 2)) + align)
    return first .. str .. second
end

function textutils.colorize_number_len(max, min, num, revert, len)
    local txtnum = ""
    if string.len(tostring(num)) > len then
	    txtnum = tostring(num):strsub(1,len);
    elseif len > string.len(tostring(num)) then
    	txtnum = textutils.align(num, len)
    else
        txtnum = tostring(num)
    end
    local color=textutils.get_number_color(max, min, num, revert)
    return '<span color='..color..'>' .. txtnum .."</span>"
end

function textutils.get_number_color(max, min, num, revert)
    local tmp = (num - min) / ((max-min)/100)
    local colors
    if revert==false then
        colors=theme.textutils.colors
    else
        colors={theme.textutils.colors[4],theme.textutils.colors[3],theme.textutils.colors[2],theme.textutils.colors[1]}
    end
    local ret
    if tmp < 20 then
        ret = colors[1]
    elseif tmp < 50 then
        ret = colors[2]
    elseif tmp < 80 then
        ret = colors[3]
    else
        ret = colors[4]
    end
    return ret
end

function textutils.colorize_number(max, min, num, revert)
    tmp = string.len(tostring(num))
    return textutils.colorize_number_len(max, min, num, revert, tmp)
end

function textutils.text_progressbar(current,length)
    local ret_text = '[ '
    for i = 1,math.floor((length/100)*(current)) do
        ret_text = ret_text .. '='
    end
    for i = 1,(length - math.floor((length/100)*(current))) do
        ret_text = ret_text .. ' '
    end
    ret_text = ret_text .. ' ]'
    return ret_text

end

function textutils.combine_arrays(arr)
    retval = {}
    for i, k in pairs(arr[1]) do
        str = ""
        for j, l in pairs(arr) do
            if (arr[j][i] ~= nil) then
                str = str .. arr[j][i]
            end
        end
        table.insert(retval,str)
    end
    return retval
end

function textutils.insertstring(str, pos, insstr)
    return (str:sub(1, pos) .. insstr .. str:sub(pos+1)) 
end

function textutils.init(awtk_theme)
    theme = awtk_theme 
end

return textutils
