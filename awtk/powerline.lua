powerline = {}
powerline.__index = powerline
local theme = nil
local utils = nil
local pwrlayout = {}
pwrlayout.__index = pwrlayout
local wibox = require("wibox")
local gears = require("gears")

-- Lain Cairo separators util submodule
-- lain.util.separators
local separators = { height = 0, width = 9 }

function separators.arrow_right(col1, col2)
    local widget = wibox.widget.base.make_widget()
    widget.col1 = col1
    widget.col2 = col2

    widget.fit = function(m, w, h)
        return separators.width, separators.height
    end

    widget.update = function(self, col1, col2)
        self.col1 = col1
        self.col2 = col2
        self:emit_signal("widget::redraw_needed")
    end

    widget.draw = function(self, wibox, cr, width, height)
        if self.col2 ~= "alpha" then
            cr:set_source_rgb(gears.color.parse_color(self.col2))
            cr:new_path()
            cr:move_to(0, 0)
            cr:line_to(width, height/2)
            cr:line_to(width, 0)
            cr:close_path()
            cr:fill()

            cr:new_path()
            cr:move_to(0, height)
            cr:line_to(width, height/2)
            cr:line_to(width, height)
            cr:close_path()
            cr:fill()
        end

        if self.col1 ~= "alpha" then
            cr:set_source_rgb(gears.color.parse_color(self.col1))
            cr:new_path()
            cr:move_to(0, 0)
            cr:line_to(width, height/2)
            cr:line_to(0, height)
            cr:close_path()
            cr:fill()
        end
   end

   return widget
end

-- Left
function separators.arrow_left(col1, col2)
    local widget = wibox.widget.base.make_widget()
    widget.col1 = col1
    widget.col2 = col2

    widget.fit = function(self, m, w, h)
        return separators.width, separators.height
    end

    widget.update = function(self, col1, col2)
        self.col1 = col1
        self.col2 = col2
        self:emit_signal("widget::redraw_needed")
    end

    widget.draw = function(self, wibox, cr, width, height)
        if widget.col1 ~= "alpha" then
            cr:set_source_rgb(gears.color.parse_color(self.col1))
            cr:new_path()
            cr:move_to(width, 0)
            cr:line_to(0, height/2)
            cr:line_to(0, 0)
            cr:close_path()
            cr:fill()

            cr:new_path()
            cr:move_to(width, height)
            cr:line_to(0, height/2)
            cr:line_to(0, height)
            cr:close_path()
            cr:fill()
        end

        if widget.col2 ~= "alpha" then
            cr:new_path()
            cr:move_to(width, 0)
            cr:line_to(0, height/2)
            cr:line_to(width, height)
            cr:close_path()

            cr:set_source_rgb(gears.color.parse_color(self.col2))
            cr:fill()
        end
   end

   return widget
end

function pwrlayout.selected(self, widget, isselected)
    for i,w in pairs(self._bgwidgets) do
        if w == widget then
            local colorid = math.fmod(i - 1, table.getn(self._colors)) + 1 
            local color = self._selcolor
            local colsep1 = colorid - 1 < 1 and self._colors[table.getn(self._colors)] or self._colors[colorid -1]
            local colsep2 = colorid + 1 > table.getn(self._colors) and self._colors[1] or self._colors[colorid + 1]

            if (isselected == false) then
                color = self._colors[colorid]
            end
            local sep1 = nil
            local sep2 = nil
            if (self._isright) then
                sep1 = self._separators[i]
                if (i + 1 <= table.getn(self._separators)) then
                    sep2 = self._separators[i + 1]
                end
            else
                sep2 = self._separators[i]
                if (i - 1 > 0) then
                    sep1 = self._separators[i - 1]
                end
            end
            if (sep1 ~= nil) then
                sep1:update(colsep1, color)
            end
            if (sep2 ~= nil) then
                sep2:update(color, colsep2)
            end
            w.bg = color
            w:emit_signal("widget::redraw_needed")
        end
    end
end

function pwrlayout.add(self, widget)
    
    local colorwd = wibox.container.background()
    local color1 = math.fmod(table.getn(self._widgets), table.getn(self._colors)) + 1 
    local color2 = 0
    table.insert(self._widgets, widget)
    if (self._isright) then
        color2 = color1 - 1 < 1 and table.getn(self._colors) or color1 - 1
        local separator = separators.arrow_left(self._colors[color2], self._colors[color1])
        table.insert(self._separators,separator)
        self._layout:add(separator)
        self._layout:add(colorwd)
    else
        color2 = color1 + 1 > table.getn(self._colors) and 1 or color1 + 1
        local separator = separators.arrow_right(self._colors[color1], self._colors[color2])
        self._layout:add(colorwd)
        table.insert(self._separators,separator)
        self._layout:add(separator)

    end
    colorwd.bg = self._colors[color1]
    colorwd.widget = widget
    colorwd:connect_signal("mouse::enter", function () 
                                                pwrlayout.selected(self,colorwd, true)
                                           end)
    colorwd:connect_signal("mouse::leave", function () 
                                                pwrlayout.selected(self, colorwd, false) 
                                            end)
    table.insert(self._bgwidgets, colorwd)

end

function pwrlayout.layout(self)
    return self._layout
end

function pwrlayout.new(colors, selcolor, isright)
    local self = setmetatable({},pwrlayout)
    self._layout = wibox.layout.fixed.horizontal()
    self._colors = {}
    for i,j in pairs(colors) do
        table.insert(self._colors, j)
    end

    self._selcolor = selcolor
    self._selected = nil
    self._widgets = {}
    self._separators = {}
    self._isright = isright
    self._bgwidgets = {}
    return self
end


function powerline.rightlayout (colors, selcolor)
    local tmp = pwrlayout.new(colors,selcolor, true)
    return tmp

end

function powerline.leftlayout (colors, selcolor)
    local tmp = pwrlayout.new(colors,selcolor, false)
    return tmp

end
function powerline.init(awtk_theme,awtk_utils)
    theme = awtk_theme
    utils = awtk_utils
end


return powerline
