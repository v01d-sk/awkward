local wibox = require("wibox")

local desklet = {}
local theme = {}
desklet.__index = desklet

function desklet.new(text, geometry)
    local self = setmetatable({}, desklet)
    self.textbox = wibox.widget.textbox()
    self.textbox:set_valign(theme.notify.align)
    local marginbox = wibox.container.margin()
    marginbox:set_widget(self.textbox)
    self.textbox:set_markup(text)
    if (geometry ~= nil and geometry.x ~= nil) then
        self._x = geometry.x
    else
        self._x = 0
    end
    if (geometry ~= nil and geometry.y ~= nil) then
        self._y = geometry.y
    else
        self._y = theme.menubar.height + 1
    end
    self.textbox:set_font(theme.desklet.font)
    self.box = wibox({ fg = theme.desklet.color.fg,
                       bg = theme.desklet.color.bg,
                       border_color = theme.desklet.color.fg,
                       border_width = 0,
                       opacity = 0.6,
                       type = "notification" })
    self.box.ontop = false

    local w, h = self.textbox:get_preferred_size (1)
	self.box:geometry({ width = w,
                        height = h,
                        x = self._x,
                        y = self._y })
    self.box:set_widget(marginbox)
    self.visible = false; 
    return self
end

function desklet.update_text(self,new_text)
    self.textbox:set_markup(new_text)
end

function desklet.hide(self)
    if (self.visible) then 
        self.box.visible=false
        self.visible=false
    end
end

function desklet.show(self)
    self.box.visible=true
    self.visible=true
end

function desklet.init(awtk_theme)
    theme = awtk_theme
end
return desklet
