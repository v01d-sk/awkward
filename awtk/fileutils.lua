local fileutils = {}

function fileutils.file_exist(name)
    if type(name)~="string" then return false end
    return os.rename(name,name) and true or false
end

return fileutils
