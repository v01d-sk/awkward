local looper = {}
looper.__index = looper

function looper.execute(self)
    for v,k in pairs(self.functions) do
        k()
    end    
end

function looper.unregister(self,id)
    self.functions[id]=nil
end

function looper.register(self,func)
    for v,k in pairs(self.functions) do
        if (k==nil) then
            self.functions[v]=func
            return v
        end
    end
    table.insert(self.functions,func)
    return #self.functions
end

function looper.new()
    local self=setmetatable({}, looper)
    self.functions = {}
    return self
end

return looper 

