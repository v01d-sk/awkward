local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")
local awtk = require("awtk")
package.path = './?.lua;' .. package.path
local driver = require("awtk.widgets.net.connman")
local net = {}
net.__index = net


function net.notify_show(self)
    local icon = ""
    local msg
    local color
    if current_service then
        local f = service_types[current_service.Type]
        if type(f) == "function" then
            icon = "panel/" .. awtk.theme.basename(f(128, current_service))
        end
        msg =        "--------------------------------\n"
        msg = msg .. "<span color='white'>        Network connection</span>\n"
        local servicename = driver:get_service_name()
        if (string.len(servicename) <= 16) then
            servicename = awtk.textutils.align_string(servicename,16)
        end
        msg = msg .. "--------------------------------\n"
        if driver:get_service_type == "wifi"  then
            msg = msg .."ESSID:\t\t<span color='white'>" .. servicename .. "</span>\n"
        end
        if (driver:get_service_error()) then
            color = awtk.theme.textutils.colors[1]
        else
            color = awtk.theme.textutils.colors[4]
        end
        msg = msg .. "Status:\t<span color=" .. color .. ">" .. awtk.textutils.align_string(current_service.State, string.len(servicename)) .. "</span>\n"
        if current_service.Type == "wifi" and show_signal[driver:service_state] then
            msg = msg .. "Signal:\t" .. awtk.textutils.colorize_number_len(100, 0, current_service.Strength, false, string.len(servicename)-1) .. "%\n" 
        end
        msg = msg .. "Interface:\t<span color='white'>" .. awtk.textutils.align_string(driver:get_iface(), string.len(servicename)) .. "</span>\n"
        msg = msg .. "IP:\t\t<span color='white'>" .. awtk.textutils.align_string(driver:get_ipaddr(), string.len(servicename)) .. "</span>\n"
    end
    if (self._notify == nil) then
        self._notify = awtk.notify.new(icon, msg)
    else
        self._notify:update_icon(icon)
        self._notify:update_text(msg)
    end
    self._notify:show(screen.primary.index)
end 

function net.notify_hide(manager)
	if (self._notify ~= nil) then
		self._notify:hide()
	end
end
function net.show(self)
	if (notify == nil) then
		self:notify_show()
	else
	    self:notify_hide()
    end
end
function net.new(driver)
    local self = setmetatable({}, net)
    self._textbox = wibox.widget
    {
        markup="",
        align="center",
        valign="center",
        font = awtk.theme.menubar.font.family,
        widget=wibox.widget.textbox,
        forced_width = (awtk.theme.menubar.font.width + 1) * 4,
    }
    self._notify = nil
	local conlayout = wibox.layout.fixed.horizontal() 
    conlayout.spacing = awtk.theme.menubar.spacing
    conlayout:add(widget)
    conlayout:add(self._textbox)
    conlayout:connect_signal("mouse::enter", function () net.notify_show(self) end)
    conlayout:connect_signal("mouse::leave", function () net.notify_hide(self) end)
    return conlayout;
end

return builder()
