local wibox = require("wibox")
local awful = require("awful")
local dock = nil
local theme = nil
local docker = {}
docker.__index = docker

function docker.create_icon(self,client)
    local imagebox = wibox.widget.imagebox()
    imagebox:set_image(awful.util.getdir("config").."/gimp/"..client.icon_name..".png")
    imagebox:connect_signal("mouse::enter", 
                            function ()
                                if (self._active) then
                                    self._active.minimized=true
                                end
                                client.minimized=false 
                                self._active=client
                            end)
    client:connect_signal("unfocus", function () client.minimized=true end)
    self._layout:add(imagebox)
end

function docker.regenerate(self)
    self._layout:reset()
    for _,c in pairs(self._clients) do
        self:create_icon(c)
    end
end

function docker.remove(self, item)

end


function docker.add(self, client)
    table.insert(self._clients, client)
    self:create_icon(client)
end

function docker.new(config)
    local self = setmetatable({}, docker)
    self._geometry={}
    self._config=config
    self._active=nil
    if config.size == "fixed" then
        if config.position == "right" then
            self._geometry.x = screen[config.screen].geometry.width - (config.iconsize + 4)
            self._geometry.width = config.iconsize + 4
            self._layout = wibox.layout.fixed.vertical()
            if (config.screen == 1) then
                self._geometry.y = theme.menubar.height
                self._geometry.height = screen[config.screen].geometry.height - theme.menubar.height 
            else
                self._geometry.y = 0
                self._geometry.height = screen[config.screen].geometry.height
            end
        elseif config.position == "left" then
            self._geometry.x = 0
            self._geometry.width = config.iconsize + 4
            self._layout = wibox.layout.fixed.vertical()
            if (config.screen == 1) then
                self._geometry.y = theme.menubar.height
                self._geometry.height = screen[config.screen].geometry.height - theme.menubar.height 
            else
                self._geometry.y = 0
                self._geometry.height = screen[config.screen].geometry.height
            end
        elseif config.position == "bottom" then
            self._layout = wibox.layout.fixed.horizontal()
            self._geometry.x = 0
            self._geometry.width = screen[config.screen].geometry.width 
            self._geometry.y = screen[config.screen].geometry.height - (config.iconsize + 4)
            self._geometry.height = config.iconsize + 4
        end
    end
    self._dock = dock.new(nil, self._layout, self._geometry)
    self._dock:set_tags(config.tags, config.screen)
    self._clients = {}
    return self
end

function docker.init(awtk_dock, awtk_theme)
    dock = awtk_dock
    theme = awtk_theme
end

return docker
