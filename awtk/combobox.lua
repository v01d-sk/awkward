local combobox = {}
local theme = {}

MatchType_Start = 0
MatchType_All = 1


combobox.__index = combobox

function combobox.get_value(self)
    return self._value
end

function combobox.set_items(self,items)
    self._items = items
end

function combobox.set_match_policy(self,policy)
    self._policy = policy
end

function combobox.value_updated(self)
    self:set_hints()
end

function combobox.set_hints(self)
    self._hints = {}
    if (self._value == "") then
        self._hints = self._items
    else
        for (v,item ipairs(self._items)) do
            if (self.policy == MatchType_Start) then
                if (string.sub(item,1,string.len(self._value))==self._value) then
                   table.insert(self._hints,item) 
                end
            elseif (self.policy == MatchType_All) then
                if string.match(item,self._value) then
                    table.insert(self._hints,item)
                end
            end
        end
    end
end

function combobox.new(value_change_callback)
    local self = setmetatable({},combobox)
    self._items = {}
    self._hints = {}
    self._menu = {}
    self._value = ""
    self._policy = MatchType_Start
    return self
end

function combobox.init(awtk_theme)
    theme = awtk_theme
end
return combobox
