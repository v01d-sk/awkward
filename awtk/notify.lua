local wibox = require("wibox")
local util = require("awful.util")
local surface = require("gears.surface")
local keygrabber = require("awful.keygrabber")
local awful = require("awful")
local notify = {}
local allnotifyobjs = {}
local theme = {}
notify.__index = notify

function notify.closeall()
    for i,item in pairs(allnotifyobjs) do
        item:hide()
    end
end


function notify.new(img,text)
    local self = setmetatable({}, notify)
    self.visible = false
    self.textbox = wibox.widget.textbox()
    self.iconbox = wibox.widget.imagebox()
    self.textbox:set_valign(theme.notify.align)
    local marginbox = wibox.container.margin()
    marginbox:set_margins(theme.notify.geometry.text_margin) 
    marginbox:set_widget(self.textbox)
    self.textbox:set_markup(text)
    self.textbox:set_font(theme.notify.font)
    self.box = wibox({ fg = theme.notify.color.fg,
                       bg = theme.notify.color.bg,
                       border_color = theme.notify.color.border,
                       border_width = theme.notify.geometry.border,
                       type = "notification" })
    img = theme.get_icon(theme.notify.geometry.icon_size,img)
    -- is the icon file readable?
    local icon
    local success, res = pcall(function() return surface.load_uncached(img) end)
    if success then
        icon = res
    else
        io.stderr:write(string.format("notify: Couldn't load image '%s': %s\n", img, res))
        icon = nil
    end
    local icon_w, icon_h = 0, 0
    if icon then 
        self.iconbox:set_resize(false)
        self.iconbox:set_image(img)
        icon_w = icon:get_width()
        icon_h = icon:get_height()
    end

    self.box.ontop = true
	local w, h = self.textbox:get_preferred_size(1)
    self._width = 0
    self._height = 0
    self._width = w + icon_w + theme.notify.geometry.text_margin * 3
    self._height = h + theme.notify.geometry.text_margin*2 + theme.notify.geometry.border*2
    if (icon_h > self._height) then
        self._height=icon_h + theme.notify.geometry.border 
    end
    local coords = mouse.coords()
    local ws = screen[1].geometry    
    self.box:geometry({ width = self._width,
                        height = self._height,
                        x = ws.x + ws.width-self._width-5,
                        y = ws.y + theme.menubar.height+1 })
    local margin = 0
    if (icon_h < self._height) then
        margin =(self._height-icon_h)/2 
    end
    local iconmargin = wibox.container.margin(self.iconbox,theme.notify.geometry.text_margin,0,margin,0)
    local layout = wibox.layout.fixed.horizontal()
    layout:add(iconmargin)
    layout:add(marginbox)
    self.box:set_widget(layout)
    table.insert(allnotifyobjs,self)
    return self
end

function notify.update_text(self,new_text)
    self.textbox:set_markup(new_text)
end

function notify.update_icon(self,new_image)
    local img = theme.get_icon(theme.notify.geometry.icon_size,new_image)
    self.iconbox:set_image(img)
end

function notify.isVisible(self)
    return self.visible
end

function notify.hide(self)
    if (self.visible) then 
        self.box.visible=false
        self.visible=false
    end
end

function notify.show(self, id)
    id = id or mouse.screen.index
    notify.closeall()
    local ws = screen[id].geometry
    self.box:geometry({ width = self._width,
                        height = self._height,
                        x = ws.x + ws.width-self._width-5,
                        y = ws.y + theme.menubar.height+1 })
    self.box.visible=true
    self.visible=true
end

function notify.init(awtk_theme)
    theme=awtk_theme
end
return notify
