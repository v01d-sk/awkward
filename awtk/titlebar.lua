local wibox = require("wibox")
local awful = require("awful")
local tagmanager = require("awtk.tagmanager")
local titlebar = {}
local callbacks = {}
local tbmap = {}
titlebar.__index = titlebar

local theme = {}

local function moveclient(titlebar)
    if (titlebar._visible) then
        awful.mouse.client.move(titlebar._client)    
    end
end

function titlebar.close_active(self)
    self._client:kill()
end

function titlebar.setstate(self,state)
	self._laststate=self._state
	self._state=state
end

function titlebar.maximize_active(self)
    print("Maximize active")
    lo = tagmanager:get_screen_layout(self._client.screen.index)
    if (lo == awful.layout.suit.max) then
        print("Max")
        self:hide()
        self._client.minimized_vertical = true
        self._client.minimized_horizontal = true
        self._client.maximized = true
        return
    end
	if (self._client.maximized) then
		self:show()
        self._client.minimized_vertical = false
        self._client.minimized_horizontal = false
	elseif (not self._client.maximized) then
		self:hide()
	end
	self._client.maximized = not self._client.maximized
	if (not self._client.maximized) then
		self:show()
		local geom = self._client:geometry()
		geom.height = geom.height + 40
		if (geom.height > screen[self._client.screen].geometry.height - 19) then
			geom.height = screen[self._client.screen].geometry.height - 19
		end
		self._client:geometry(geom)
	end
    print ("Visible: " .. (self._visible and "true" or "false"))
end

function titlebar.minimize_active(self)
    self._client.minimized = true
end

function titlebar.isvisible(self)
    return self._visible
end

function titlebar.get_layout(self)
    return self._main_layout
end

function titlebar.hide(self)
    self._visible=false
    awful.titlebar.hide(self._client)
end

function titlebar.show(self)
    lo = tagmanager:get_screen_layout(self._client.screen.index)
    if (lo == awful.layout.suit.max and not self._client.dockable) then
        self._visible=true
        awful.titlebar.show(self._client)
    end
end

function titlebar.update(self)
    if self._focuschange then
        if self._active then
            self._button_close:set_image(theme.get_icon(24,"window-close"))
            if (not self._client.skip_taskbar) then
                self._button_minimize:set_image(theme.get_icon(24,"window-minimize"))
            end
            self._button_maximize:set_image(theme.get_icon(24,"window-maximize"))
        else
            self._button_close:set_image(theme.get_icon(24,"window-close-inactive"))
            if (not self._client.skip_taskbar) then
                self._button_minimize:set_image(theme.get_icon(24,"window-minimize-inactive"))
            end
            self._button_maximize:set_image(theme.get_icon(24,"window-maximize-inactive"))
        end
    end
    self._focuschange = false
    if (tagmanager:ismoving() == false) then
        if self._client.type == "dialog" then
            self:show()
            self._client.maximized = false
            self._client.maximized_vertical = false
            self._client.maximized_horizontal = false
            self._client.floating = true
            return
        end
        lo = tagmanager:get_screen_layout(self._client.screen.index)
        if (lo == awful.layout.suit.max) then
            print("Max")
            self:hide()
            self._client.maximized_vertical = true
            self._client.maximized_horizontal = true
            self._client.maximized = true
            return
        end
        if (self._client ~= nil) then
            if ((lo == awful.layout.suit.floating or
                self._client.floating) and
               not self._client.maximized) then
                awful.titlebar.show(self._client)
            elseif lo == awful.layout.suit.max then
                awful.titlebar.hide(self._client)
                self._client.maximized = true
            else
                awful.titlebar.hide(self._client)
            end
        end
    end
end

function titlebar.new(client, max_callback)
    local self = setmetatable({}, titlebar)
	self.maximized=1
	self.normal=2
	self.fullscreen=3
    self._max_callback=max_callback
    self._client = client
	self._state = self.normal
	self._laststate=self._state	
    self._visible = true;
    self._main_layout = wibox.layout.align.horizontal()
	self._main_layout.expand = "none"
    self._right_layout = wibox.layout.fixed.horizontal()
    self._middle_layout = wibox.layout.fixed.horizontal()
    self._left_layout = wibox.layout.fixed.horizontal()
    self._name = client.name
    self._app_icon = wibox.widget.imagebox()
    self._button_close = wibox.widget.imagebox()
    self._button_minimize = wibox.widget.imagebox()
    self._button_maximize = wibox.widget.imagebox()
    self._active = true
    self._button_close:buttons(awful.util.table.join(
        awful.button({ }, 1, function () self:close_active() end)
    ))
    self._button_maximize:buttons(awful.util.table.join(
        awful.button({ }, 1, function () self:_max_callback() end)
    ))
    self._button_minimize:buttons(awful.util.table.join(
        awful.button({ }, 1, function () self:minimize_active() end)
    ))
    self._left_layout:add(self._app_icon)
    
    self._middle_layout:add(awful.titlebar.widget.titlewidget(self._client))

    self._right_layout:add(self._button_minimize)
    self._right_layout:add(self._button_maximize)
    self._right_layout:add(self._button_close)
    if (self._client.class) then
	    if (self._client.class:find("chromium")) then
		    icon="chromium-browser"
        else
            icon=string.lower(self._client.class)
        end
	    self._app_icon:set_image(theme.get_icon(24, 'apps/' .. icon))
    end
    self._button_close:set_image(theme.get_icon(24,"window-close"))
    if (not self._client.skip_taskbar) then
	    self._button_minimize:set_image(theme.get_icon(24,"window-minimize"))
    end
    self._button_maximize:set_image(theme.get_icon(24,"window-maximize"))
    self._main_layout:set_left(self._left_layout)
    self._main_layout:set_middle(self._middle_layout)
    self._main_layout:set_right(self._right_layout)
    awful.titlebar(self._client):set_widget(self._main_layout)   
    self._main_layout:buttons(awful.util.table.join(
        awful.button({ }, 1, function () moveclient(self) end)
        ))

    tbmap[client] = self;
    return self
end

function titlebar.getclient(self)
    return self._client;
end

function titlebar.get(c)
    return tbmap[c]
end
function titlebar.init(awtk_theme)
    theme = awtk_theme
end

function titlebar.unmanage(c)
    if tbmap[c] ~= nil then
        tbmap[c] = nil
        for _,callback in pairs(callbacks["unmanage"]) do
            callback(c)
        end
    end
end

function titlebar.connect_signal(signal, callback)
    if (callbacks[signal] == nil) then
        callbacks[signal] = {}
    end
    table.insert(callbacks[signal], callback)
end

client.connect_signal("unmanage", function (c) titlebar.unmanage(c) end)
client.connect_signal("focus", function (c) 
                                    tb = titlebar.get(c)
                                    if (tb) then
                                        tb._active = true
                                        tb._focuschange = true
                                        tb:update() 
                                    end
                               end)
client.connect_signal("unfocus", function (c)
                                      tb = titlebar.get(c)
                                      if (tb) then
                                          tb._active = false
                                          tb._focuschange = true
                                          tb:update() 
                                      end
                                  end)
return titlebar
