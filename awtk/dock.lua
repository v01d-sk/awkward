local wibox = require("wibox")
local awful = require("awful")
local dock = {}
dock.__index = dock
local theme= {}
local tagmanager = nil
function dock.hint(self)
    
end

function dock.hide(self)
    self._box.visible=false
end

function dock.delete(self)
    tagmanager.get_instance():deletedock(self)
    self:hide()
end

function dock.click(self)
    if (self._client ~= nil) then
        self._client.minimized = not self._client.minimized
    end
end

function dock.show(self)
    self._box.visible=true
end

function dock.set_tags(self,tags, screen)
    for i = 1,  table.getn(tags) do
        tagmanager.get_instance():adddock(self,tags[i],screen)
    end
end

function dock.new(client, layout, geometry)
    local self = setmetatable({}, dock)
    self._client = client

    self._box = wibox({fg = theme.desklet.color.fg,
                       bg = theme.desklet.color.bg,
                       border_color = theme.desklet.color.fg,
                       border_width = 0,
                       type = "notification" })
    self._box.above = true
    self._box:geometry({ width = geometry.width,
                        height = geometry.height,
                        x = geometry.x,
                        y = geometry.y })
    self._box:set_widget(layout)
    self._enter=false

    if (self._client ~= nil) then
        self._box:buttons(awful.util.table.join(
            awful.button({ }, 1, function () dock.click(self) end)
        ))
        local tags =self._client:tags()
        self:set_tags(tags,self._client.screen)
        self._box:connect_signal("mouse::enter", function () self._client.minimized = false end)
        self._client:connect_signal("unfocus", function () self._client.minimized=true end)
        self._client:connect_signal("unmanage", function () self:delete() end)
    end

    return self
end

function dock.init(awtk_tagmanager, awtk_theme)
    tagmanager = awtk_tagmanager
    theme = awtk_theme
end

return dock
