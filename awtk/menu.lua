local wibox = require("wibox")
local awful = require("awful")
local keygrabber = require("awful.keygrabber")
local surface = require("gears.surface")

local utils = {}
local theme = {}

local menuitem = {}
menuitem.__index = menuitems
local menu = {}
menu.__index = menu
menu.menu_keys = {  up = { "Up" },
                    down = { "Down" },
                    back = { "Left" },
                    exec = { "Return" },
                    enter = { "Right" },
                    close = { "Escape" } 
                }

local function processtext(str)
    if (str) then
        pos,tmp = string.find(str,"&")
        if pos ~= nil then
            return str:sub(pos+1,pos+1), str:sub(1, pos - 1) .. '<u>' .. str:sub(pos + 1, pos+1) .. '</u>' .. str:sub(pos+2)
        end
    end
    return nil,str
end

function menuitem.set_size(self,size)
    self._width = size.width
    self._height = size.height
end

function menuitem.get_key(self)
    return self._key
end

function menuitem.calculate_required_size(self)
    local width = 0
    local height = theme.menu.geometry.icon_size + 2*theme.menu.geometry.margin + 4
	self._tw,self._th = self._textbox:get_preferred_size(1)
    local calcwidth
    local icons_count = 2
    if (not self._properties.enable_submenu) then
        icons_count = 1
    end
    if (self._icon ~= nil) then
        calcwidth = self._tw + (theme.menu.geometry.margin*6) + icons_count*theme.menu.geometry.icon_size
    else 
        calcwidth = self._tw + self._first_margin_size+ 3 * theme.menu.geometry.margin
    end
    if calcwidth < theme.menu.geometry.minimal_width then
        width = theme.menu.geometry.minimal_width
    else
        width = calcwidth
    end
    return width, height
end

function menuitem.new(properties)
    local self = setmetatable({}, menuitem)
    self.submenu=nil
    self._properties=properties
    self._key,self._text = processtext(properties.text)
    if (properties.type ~= "separator") then
        self._textbox = wibox.widget.textbox()
        self._textbox:set_font(theme.menu.font)
        self._textbox:set_markup(self._text)
        if (properties.icon) then
            self._icon = wibox.widget.imagebox(properties.icon)
            self._icon:set_resize(false)
			self._icon.forced_height = theme.menu.geometry.icon_size
			self._icon.forced_width = theme.menu.geometry.icon_size
            self._first_margin_size = 0
        else
            self._icon = nil
            self._first_margin_size = theme.menu.geometry.margin * 3 + theme.menu.geometry.icon_size
        end
    end
    return self
end

function menuitem.sel(self)
    if (self._properties.type ~= "separator") then
        self._bg.bg = theme.menu.color.select_bg
    end
end

function menuitem.unsel(self)
    self._bg.bg = theme.menu.color.bg
    self._bg:emit_signal("widget::redraw_needed")
end

function menuitem.lclick_callback(self)
    menu.exec(self._properties.owner,self._properties.id)
end

function menuitem.on_mouse_over(self)
    self._properties.on_mouse_over(self._properties.owner,self._properties.id)
end

function menuitem.get_type(self)
    return self._properties.type
end

function menuitem.get_layout(self)
    self._bg = wibox.container.background()
    self._layout = wibox.layout.fixed.horizontal()
    self._bg.widget = self._layout
    local tmplayout = wibox.layout.fixed.horizontal()
    if (self._properties.type ~= "separator") then
        local marginicon = nil
        if (self._icon) then
            self._marginicon = wibox.container.margin(self._icon,theme.menu.geometry.margin,
                                                              theme.menu.geometry.margin,
                                                              theme.menu.geometry.margin,
                                                              theme.menu.geometry.margin)

            tmplayout:add(self._marginicon)
        end
        local first_margin = theme.menu.geometry.margin
        local last_margin = theme.menu.geometry.margin
        if (self._icon == nil) then
            first_margin = self._first_margin_size; 
        end
    -- fill space for menu icon_size
		local w,h = self._textbox:get_preferred_size (1)
        last_margin = self._width - first_margin - w - 4
        if (self._icon) then
            last_margin = last_margin - (2*theme.menu.geometry.margin + theme.menu.geometry.icon_size)
        end
    -- last_margin = 2*self._theme.menu.geometry.margin + self._theme.menu.geometry.icon_size
        if (self._properties.type == "menu") then 
            self._submenu = wibox.widget.imagebox()
            self._submenu:set_image(theme.get_icon(theme.menu.geometry.icon_size,"actions/arrow-right"))
            self._submenu:set_resize(false)
            self._submenumargin = wibox.container.margin(self._submenu,theme.menu.geometry.margin,
                                                                    theme.menu.geometry.margin, 
                                                                    theme.menu.geometry.margin, 
                                                                    theme.menu.geometry.margin)
            last_margin = last_margin - (2*theme.menu.geometry.margin + theme.menu.geometry.icon_size)

        end
        self._margintext = wibox.container.margin(self._textbox,first_margin,
                                                             last_margin,
                                                             theme.menu.geometry.margin,
                                                             theme.menu.geometry.margin)
        tmplayout:add(self._margintext)
        if (self._submenumargin) then
            tmplayout:add(self._submenumargin)
        end
        self._marginlayout = wibox.container.margin(tmplayout,2,2,2,2) 
        self._layout:add(self._marginlayout)
        self._layout:connect_signal("mouse::enter", function () menuitem.on_mouse_over(self) end)
        self._layout:buttons(awful.util.table.join(
            awful.button({ }, 1, function () menuitem.lclick_callback(self) end)
        ))
    else
        tmpprogress = wibox.widget.progressbar() 
        tmpprogress.forced_width = self._width-8
        tmpprogress.forced_height = 1
        tmpprogress.value = 1
        tmpprogress.color = theme.menu.color.fg
        self._marginlayout = wibox.container.margin(tmpprogress,3,3,3,3)

        self._layout:add(self._marginlayout)
    end
    return self._bg
end

------------------------ KEY HANDLING ---------------------------

local function grabber(menuobj, mod, key, event)
    local tmpmod
    local tmpkey
    tmpmod, tmpkey = menu.getkey(menuobj)
    if event ~= "press" then return end
    if (tmpmod and mod[2] == tmpmod) then
        if (key == tmpkey) then
            menu.close_all(menuobj)
            return
        end
    end
    if (mod[2]) then
        return
    end
    if  key == "Up" then
        menu.select_dec(menuobj)
    elseif key == "Down" then
        menu.select_inc(menuobj)
    elseif key == "Right" then
        menu.show_submenu(menuobj)
    elseif key == "Return" then
        menu.exec(menuobj)
    elseif key == "Left" then
        menu.hide(menuobj)
    elseif key == "Escape" then
        menu.close_all(menuobj)
    end
end

------------------------ MENU CLASS ------------------------------

function menu.getkey(self)
    return self._modkey, self._key
end

function menu.select_dec(self)
    local sitem = 0
    if (self._selected==1) then
        s_item = self._itemscount
    elseif(self._selected==0) then
        s_item = 1
    else
        s_item = self._selected -1
    end
    if (menuitem.get_type(self._menuitems[s_item])=="separator") then
        s_item = s_item -1
    end
    self:select_item(s_item)
end

function menu.select_inc(self)
    local sitem = 0
    if (self._selected==self._itemscount) then
        s_item = 1
    else
        s_item = self._selected + 1
    end
    if (menuitem.get_type(self._menuitems[s_item])=="separator") then
        s_item = s_item + 1
    end
    self:select_item(s_item)
end

function menu.show_submenu(self)
    if (self._selected == 0) then return end
    if (menuitem.get_type(self._menuitems[self._selected])=="menu") then
        local menu_props = { x=self._x + 150,
                             y=self._y + self._ysizes[self._selected],
                           }
        menu.set_properties(self._callbacks[self._selected],menu_props)
        menu.show(self._callbacks[self._selected])
    end
end

function menu.exec(self)
    if (self._selected == 0) then return end
    if (menuitem.get_type(self._menuitems[self._selected])=="menu") then
        local menu_props = { x=self._x + 150,
                             y=self._y + self._ysizes[self._selected],
                           }
        menu.set_properties(self._callbacks[self._selected],menu_props)
        menu.show(self._callbacks[self._selected])
    else
        if (type(self._callbacks[self._selected])=="string") then
            awful.spawn.with_shell(self._callbacks[self._selected])
            menu.close_all(self)
        elseif(type(self._callbacks[self._selected])=="function") then
            self._callbacks[self._selected]()
        else

        end
    end
end

function menu.on_mouse_over(self,id)
        if (self._selected ~= 0) then
            menuitem.unsel(self._menuitems[self._selected])
            if (menuitem.get_type(self._menuitems[self._selected])=="menu") then
                menu.hide(self._callbacks[self._selected])
                --self:show()
            end
        end
        self._selected=id
        menuitem.sel(self._menuitems[id])
        if (menuitem.get_type(self._menuitems[id])=="menu") then
            local menu_props = { x=self._x + 150,
                                 y=self._y + self._ysizes[id],
                                }
            menu.set_properties(self._callbacks[id],menu_props)
            self._submenu_active=true 
            menu.show(self._callbacks[id])
        end
end

function menu.select_item(self,id)
    if (self._visible and self._selected ~= 0) then
        menuitem.unsel(self._menuitems[self._selected])
        if (menuitem.get_type(self._menuitems[self._selected])=="menu") then
            menu.hide(self._callbacks[self._selected])
        end
        menuitem.sel(self._menuitems[id])
    end
    self._selected=id
end

function menu.close_all(self)
    menu.hide(self)
    if (self._parent) then 
        menu.close_all(self._parent)
    end
end

function menu.new(menuitems,properties)
    local self = setmetatable({}, menu)
    self._visible=false
    self._calculated = false
    self._keygrabber = function (mod, key, event)
                            grabber(self, mod, key, event)
                       end    
    self._ysizes = {}
    self._submenu_active=false
    self._properties = properties
    if (properties.modkey and properties.key) then 
        self._modkey = properties.modkey
        self._key = properties.key
    end
    self._width = 0
    self._height = 0
    self._size = 0
    self._selected = 0
    self._mouse_close = false;
    self._callbacks = {}
    if (properties.parent) then
        self._parent = properties.parent
    end
    self._box = wibox({ fg = theme.menu.color.fg,
                        bg = theme.menu.color.bg,
                        border_color = theme.menu.color.border,
                        border_width = theme.menu.geometry.border,
                        type = "menu" })
    self._box.ontop = true
    self._main_layout = wibox.layout.fixed.vertical()
    self._sideimage = nil
    if (properties.side_bar) then
        -- create sidebar
        self._sideimage = properties.side_bar
    end
    if (properties.mouse_close) then
        self._mouse_close = true
    end
    self._menuitems = {}
    for v,item in pairs(menuitems) do
        local item_props = {}
        if (item[1] ~= "-") then
            item_props.text = item[1]
            if (item[2] ~= nil) then
                if (type(item[2]) == "string") then 
                    if utils.file_exist(icon) then
                        -- menu icons
                        item_props.icon = item[2]
                    else
                        item_props.icon = theme.get_icon(theme.menu.geometry.icon_size,item[2])
                        if (not utils.file_exist(item_props.icon)) then
                            item_props.icon = nil
                        end
                    end
                else
                    -- add free space
                    item_props.icon = nil
                end
            else
                item_props.icon = nil;
            end
            if (type(item[3]) == "table"  and self._properties.enable_submenu==true) then
                item_props.type = "menu"
                local submenu_props={}
                if (properties.modkey and properties.key) then
                    submenu_props = {mouse_close=true,
                                     parent=self,
                                     modkey=properties.modkey,
                                     key=properties.key}
                else 
                    submenu_props = {mouse_close=true,
                                     parent=self}
                end
                local menu = menu.new(item[3],submenu_props)
                table.insert(self._callbacks,menu)
            else
                table.insert(self._callbacks,item[3])
            end
            item_props.callback = menu.itemclick
            item_props.on_mouse_over = menu.on_mouse_over
            item_props.id = v
            item_props.owner = self
            item_props.enable_submenu = self._properties.enable_submenu
            local item = menuitem.new(item_props)
            local w,h = menuitem.calculate_required_size(item)
            if (w > self._width) then
                self._width = w
            end
            if (h > self._height) then
                self._height = h
            end
            table.insert(self._menuitems,item)
        else
            --insert separator
            item_props.text = ""
            item_props.type = "separator"
            local item = menuitem.new(item_props)
            table.insert(self._menuitems,item)
            table.insert(self._callbacks,"")
        end
        self._itemscount = v
    end
    return self
end

function menu.is_visible(self)
    return self._visible
end

function menu.set_properties(self,properties)
    if (self._calculated) then
        if (properties.x) then
            self._x = properties.x
        end
        if (properties.y) then
            self._y = properties.y
        end
        self._box:geometry({x=self._x,
                            y=self._y,
                            width = self._width+4,
                            height = self._size+4})
    end
    self._properties = properties
end

function menu.show(self)
    local count
    self._visible=true
    if self._calculated == false then
        self._size=0
        for v, item in pairs(self._menuitems) do
            menuitem.set_size(item,
                             {width = self._width, 
                              height = self._height})
            
            self._main_layout:add(menuitem.get_layout(item))
            table.insert(self._ysizes,self._size)
            if (menuitem.get_type(item)~="separator") then
                self._size=self._size + self._height
            else
                self._size = self._size + 6
            end
        end
        self._size = self._size + 2*theme.menu.geometry.border
        if (self._properties == nil or self._properties.x == nil) then
            local coords = mouse.coords()
            self._x = coords.x
            self._y = coords.y
        else
            self._x = self._properties.x
            self._y = self._properties.y
        end 
        ws = screen[1].workarea
        if (self._y + self._size + 4 > ws.height) then
            self._y = ws.height - self._size - 5
        end
        self._main_margin = wibox.container.margin(self._main_layout,2,2,2,2)
        if (self._sideimage) then
            local topspace=0
            sidelayout = wibox.layout.fixed.horizontal()
            sideimagebox = wibox.widget.imagebox()
            local success, icon = pcall(function() return surface.load_uncached(self._sideimage) end)
            if (success) then
                icon_w = icon:get_width()
                icon_h = icon:get_height()
                self._width = self._width + icon_w +2
                if (self._size <= icon_h) then 
                    self._size = icon_h
                else
                    topspace = self._size - icon_h 
                end
                sideimagebox:set_image(self._sideimage)
            end
            sidemargin =  wibox.container.margin(sideimagebox,0,0,topspace,0)
            sidelayout:add(sidemargin)
            sidelayout:add(self._main_margin)
            self._box:set_widget(sidelayout)
        else    
            self._box:set_widget(self._main_margin)
        end
        if (self._x + self._width+15 > ws.x + ws.width) then
            if (self._properties.enable_submenu) then
                self._x = ws.x + ws.width - self._width -85
            else
                self._x = ws.x + ws.width - self._width -15
            end
        end
        self._box:geometry({x=self._x,
                            y=self._y,
                            width = self._width+4,
                            height = self._size+4})
        if (self._mouse_close) then
            self._box:connect_signal("mouse::enter", 
                    function()
                        self._box:connect_signal("mouse::leave", 
                            function()
                                if (self._submenu_active==false) then
									self:hide()
                                end
                            end)
                    end)
        end
    end
    if (self._selected ~= 0) then
        menuitem.sel(self._menuitems[self._selected])
    end
    self._calculated=true
    keygrabber.run(self._keygrabber)
    self._box.visible = true
end

function menu.hide(self)
    keygrabber.stop(self._keygrabber)
    self._box.visible = false
    if (self._selected ~= 0) then
        if (menuitem.get_type(self._menuitems[self._selected])=="menu") then
            menu.hide(self._callbacks[self._selected])
        end
        menuitem.unsel(self._menuitems[self._selected])
    end
    self._selected = 0
    self._visible=false
end

function menu.init(awtk_theme,awtk_utils)
    theme = awtk_theme
    utils = awtk_utils
end

return menu
