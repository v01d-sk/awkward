local wibox = require("wibox")
local awful = require("awful")
local util = require("awful.util")
local surface = require("gears.surface")
local controlbar = {}
local allcontrolobjs = {}
local theme = {}

controlbar.__index = controlbar

function controlbar.set_value(self,value)
    self._progressbar:set_value(value/100)
end
function controlbar._get_percentage(self)
    local coords = mouse.coords()
    return math.floor(((coords.x - (self._geometry.x + 
                                3*theme.controlbar.geometry.margin + 
                                theme.controlbar.geometry.icon_size +
                                theme.controlbar.geometry.border))/
                                self._progressbar_size) * 100)

                            end
function controlbar._progress_callback(self,button)
    if (self._callbacks.progressbar ~= nil) then
        self._callbacks.progressbar(button, controlbar._get_percentage(self))
    end
end

function controlbar._icon_callback(self,button)
    if (self._callbacks.icon ~= nil) then 
        self._callbacks.icon(button)
    end
end

function controlbar.set_callbacks(self,prog_callback, icon_callback)
    self._callbacks.progressbar = prog_callback
    self._callbacks.icon = icon_callback
end

function controlbar.new(img,mygeometry)
    local self = setmetatable({}, controlbar)
    self._icon = theme.get_icon(theme.controlbar.geometry.icon_size,img)
    self._is_visible=false
    self._iconbox = wibox.widget.imagebox()
    self._iconbox:set_resize(false)
    self._geometry = mygeometry
    self._iconbox:set_image(self._icon)
    self._widget = wibox( {fg = theme.controlbar.color.fg,
                          bg = theme.controlbar.color.bg,
                          border_color = theme.controlbar.color.border,
                          border_width = theme.controlbar.geometry.border})
    self._progressbar = wibox.widget.progressbar()
    self._widget.ontop=true
    self._callbacks = {
        progressbar=nil,
        icon=nil,
    }
    self._progressbar:set_background_color(theme.controlbar.color.progress_bg)
    self._progressbar:set_border_color(theme.controlbar.color.progress_border)
    self._progressbar:set_color(theme.controlbar.color.progress_fg)
    print (self._icon)
    self._marginprogress = wibox.container.margin(self._progressbar,
                                               theme.controlbar.geometry.margin,
                                               theme.controlbar.geometry.margin,
                                               theme.controlbar.geometry.progress_margin,
                                               theme.controlbar.geometry.progress_margin)
    self._marginicon = wibox.container.margin(self._iconbox,
                                           theme.controlbar.geometry.margin,
                                           theme.controlbar.geometry.margin,
                                           theme.controlbar.geometry.icon_margin,
                                           theme.controlbar.geometry.icon_margin)
    self._widget:geometry({ width = mygeometry.width,
                            height = mygeometry.height,
                            x = mygeometry.x,
                            y = mygeometry.y })
    self._progressbar_size = mygeometry.width - 
                             theme.controlbar.geometry.icon_size - 
                             4*theme.controlbar.geometry.margin - 
                             theme.controlbar.geometry.border 
    self._progressbar.fixed_width = self._progressbar_size
    self._progressbar.fixed_height = (mygeometry.height-2)-(2*theme.controlbar.geometry.progress_margin)

    self._layout = wibox.layout.fixed.horizontal()
    self._layout:add(self._marginicon)
    self._layout:add(self._marginprogress)
    self._widget:set_widget(self._layout) 
    
    self._progressbar:buttons(awful.util.table.join(
            awful.button({ }, 1, function () controlbar._progress_callback(self,1) end),
            awful.button({ }, 2, function () controlbar._progress_callback(self,2) end),
            awful.button({ }, 3, function () controlbar._progress_callback(self,3) end),
            awful.button({ }, 4, function () controlbar._progress_callback(self,4) end),
            awful.button({ }, 5, function () controlbar._progress_callback(self,5) end)
    ))
    self._marginicon:buttons(awful.util.table.join(
            awful.button({ }, 1, function () controlbar._icon_callback(self,1) end),
            awful.button({ }, 2, function () controlbar._icon_callback(self,2) end),
            awful.button({ }, 3, function () controlbar._icon_callback(self,3) end),
            awful.button({ }, 4, function () controlbar._icon_callback(self,4) end),
            awful.button({ }, 5, function () controlbar._icon_callback(self,5) end)
    ))
    return self    
end

function controlbar.set_icon(self,icon)
    self._icon = theme.get_icon(theme.controlbar.geometry.icon_size,icon)
    self._iconbox:set_image(self._icon)
end

function controlbar.show(self)
    self._widget.visible=true
    self._is_visible=true
end

function controlbar.hide(self)
    self._widget.visible=false
    self._is_visible=false
end

function controlbar.is_visible(self)
    return self._is_visible
end

function controlbar.init(awtk_theme)
    theme=awtk_theme
end
return controlbar
