
local awtk = {}
local initialized=false
local loop = require ("awtk.looper");
local gears = require ("gears")

if (initialized == false) then
    initialized = true
    awtk.looper = loop.new()
    awtk.fileutils = require ("awtk.fileutils")
    awtk.theme = require ("awtk.theme")
    awtk.tagmanager = require("awtk.tagmanager")
    awtk.notify = require ("awtk.notify")
    awtk.textutils = require ("awtk.textutils")
    awtk.controlbar = require ("awtk.controlbar")
    awtk.menu = require("awtk.menu")
    awtk.titlebar = require("awtk.titlebar")
    awtk.desklet = require("awtk.desklet")
    awtk.powerline = require("awtk.powerline")
--    awtk.widgets = require("awtk.widgets")
    --awtk.combobox = require("awtk.combobox")
    awtk.tagmanager.init(awtk.theme)
    awtk.notify.init(awtk.theme)
    awtk.textutils.init(awtk.theme)
    awtk.controlbar.init(awtk.theme)
    awtk.menu.init(awtk.theme,awtk.fileutils)
    awtk.titlebar.init(awtk.theme)
    awtk.desklet.init(awtk.theme)
    awtk.powerline.init(awtk.theme)
    --awtk.combobox.init(awtk.theme)
    awtk.__sectimer = gears.timer({ timeout = 1})
    awtk.__sectimer:connect_signal("timeout", function () awtk.looper:execute() end)
    awtk.__sectimer:start()
end

return awtk
