local awful = require ("awful")
local icon_theme = require("menubar.icon_theme")
local menubar = require("menubar")

--define default theme
local theme = {
    default_iconset = awful.util.getdir("config").."/icons/Blue/",
    iconset = "/usr/share/icons/Papirus-Dark/",
    menubar = {
        color = {
            bg = "#111111",
            bg2 = "#282828",
            selected = "#454881",
        },
        height = 20, 
        spacing = 3,
        font = {
            family = "AnonymicePro Nerd Font 12",
            width = 8,
            height = 12
        }

    },
    titlebar = {
        color = {
            focus = {
                fg = "#FFFFFF",
                bg = "#0a0a0a",
                border = "#AAAAAA",
            },
            unfocus = {
                fg = "#909090",
                bg = "#101010",
                border = "#AAAAAA",
            },
        },
        geometry = {
            icon_size = 24,
            border = 1,
            height = 24,
        }
    },
    menu = {
        color = {
            fg = "#FFFFFF",
            bg = "#0a0a0a",
            border = "#AAAAAA",
            select_bg = "#454881",
        },
        geometry = {
            icon_size = 24,
            margin = 3,
            border = 1,
            minimal_width = 120,
        },
        font = "sans 10",
    },
    controlbar = {
        color = {
            fg = "#909090",
            bg = "#0a0a0a",
            border = "#AAAAAA",
            progress_fg = "#22999a",
            progress_bg = "#1b1b1b", 
            progress_border = "#505050",
        },
        geometry = {
            border = 1,
            margin = 5,
            progress_margin = 10,
            icon_margin = 2,
            icon_size = 24,
        },
    },
    notify = {
        color = {
            fg = "#909090",
            bg = "#0a0a0a",
            border = "#AAAAAA"
        },
        geometry = {
            border = 1,
            text_margin = 10,
            icon_margin = 0,
            icon_size = 128,
        },
        font = "AnonymicePro Nerd Font 16",
        align = "middle",
    },
    desklet = {
        color = {
            fg = "#909090",
            bg = "#000000",
        },
        font = "monospace 12",
        align = "middle",
    },
    widget = {
        progressbar = {
            color = {
                fg = "#b7b7b7",
                bg = "#1b1b1b", 
                border = "#505050",
            },
            geometry = {
                width = 8,
                height = 10,
            }

        }
    },
    textutils = {
        colors={'"#c14d4d"','"#e4aa6c"','"#9eab35"','"#28a148"'}
    }
}

local function basename(str)
	local name = string.gsub(str, "(.*/)(.*)", "%2")
	local found, len, remainder = string.find(name, "^(.*)%.[^%.]*$")
	if found then
		if name:len() - remainder:len() == 4 then
			return remainder
		end
	end	
	return name
end

function theme.basename(str)
    return basename(str)
end

function theme.get_icon(size,name)
	retval = theme.iconset..size..'x'..size..'/'..name
	if (not name:match(".png") and 
		not name:match(".xpm") and
        not name:match(".svg") and
		not name:match(".gif")) then
		retval = retval .. ".svg"
	end
	fd = io.open(retval)
	if (fd == nil) then
        retval = theme.default_iconset..size..'x'..size..'/'..basename(name)..".png"
        fd = io.open(retval)
        if (fd == nil) then
            retval = theme.default_iconset..size..'x'..size..'/empty.png'
        end
    end
    if (fd) then
        fd:close()
    end
    return retval
end

function theme.check_icon(size,name)
    local fd = io.open (name)
    retval = true
    if (fd == nil) then
        local filename = theme.iconset..size..'x'..size..'/'..name
        if (not name:match(".png") or 
            not name:match(".xpm") or
            not name:match(".svg") or
            not name:match(".gif")) then
            filename = filename .. ".png"
        end
        fd = io.open(filename)
    end
    if (fd == nil) then
        retval = false
    else
        fd:close()
    end
    return retval
end

return theme
