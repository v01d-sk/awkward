local awful = require("awful")
local wibox = require("wibox")
local awtk_theme = nil
local tagmanager = {}
local menu = require("awtk.menu")
tagmanager.__index = tagmanager

local tagmanager_obj = nil

local capi = {
    screen = screen
}

local function salvage(tag)
    -- The screen to move the orphaned tag to.
    local newscreen = capi.screen.primary

    -- Make sure the tag isn't selected when moved to the new screen.
    tag.selected = false

    tagmanager.movetag(tag, newscreen)

    capi.screen[newscreen]:emit_signal("tag::history::update")
end

function tagmanager.taglist(self, i)
    return self._taglists[i].widget
end

function tagmanager.layoutlist(self, i)
    return self._layoutlists[i].widget
end
function tagmanager._addtag(self, props)
    local tag = {}
    tag.tag = awful.tag.add((props.texticon == "") and props.name or props.texticon .. " " .. props.name,
                            {(props.screen and props.screen <= capi.screen.count()) and screen or capi.screen.primary,
                             layout = self._layouts[props.layout].layout})
    if (props.screen and props.screen <= capi.screen.count()) then
        self:movetag(tag.tag, props.screen)
    end
    tag.icon = props.icon
    tag.textname = props.name
    tag.tag:connect_signal("request::screen", salvage)
    if (props.layout > 0 and props.layout <= table.getn(self._layouts)) then
        tag.layout = props.layout
    end
    table.insert(self._tagmenudata,{props.name,
                                    props.icon,
                                    function ()
                                        tagmanager.activate_tag(self, tag)
                                        tagmanager.close(self)
                                    end})
    return tag
end

function tagmanager.get_screen_layout(self, id)
    id = id or awful.screen.focused().index
    return self._taglists[id].tag.tag.layout
end

function tagmanager.activate_tag(self, tag, id)
    id = id or awful.screen.focused().index
    self._movingscreen = true
    for i, t in pairs(self._taglists) do
        if t.tag == tag then
            self._taglists[i].tag = self._taglists[id].tag
            self._taglists[i].textbox:set_markup(self._taglists[i].tag.textname)
            self._taglists[i].icon:set_image(awtk_theme.get_icon(24,self._taglists[i].tag.icon))
            self:activate_layout(self._taglists[i].tag.layout, i)
            self:movetag(self._taglists[i].tag.tag,i)
            self._taglists[i].tag.tag:view_only()
            break
        end
    end
    self._taglists[id].tag = tag
    self._taglists[id].textbox:set_markup(self._taglists[id].tag.textname)
	self._taglists[id].icon:set_image(awtk_theme.get_icon(24,self._taglists[id].tag.icon))
    self:activate_layout(self._taglists[id].tag.layout, id)
    self:movetag(tag.tag,id)
    self._taglists[id].tag.tag:view_only()
    self._movingscreen = false
end

function tagmanager.ismoving(self)
    return self._movingscreen
end

function tagmanager.activate_layout(self, layoutid, id)
    id = id or awful.screen.focused().index
    self._taglists[id].tag.tag.layout = self._layouts[layoutid].layout
    self._taglists[id].tag.layout = layoutid
    self._layoutlists[id].textbox:set_markup(self._layouts[layoutid].name)
    self._layoutlists[id].icon:set_image(awtk_theme.get_icon(24, self._layouts[layoutid].icon))

end

function tagmanager.close(self)
    self._tagmenu:hide()
    self._layoutmenu:hide()
end


function tagmanager.layoutlistshow(self)
    local id =  awful.screen.focused().index
    if (self._layoutmenu:is_visible()==false) then
        self:close()
        for i,t in pairs(self._tags) do
            if t == self._taglists[id].tag then
                self._layoutmenu:select_item(t.layout)
            end
        end

        local ws = capi.screen[id].geometry
        local x = 70
        if capi.screen[id] == capi.screen.primary then
            x = 120
        end

        local menu_props = {x=ws.x + x,
                            y=ws.y + awtk_theme.menubar.height + 1}
        self._layoutmenu:set_properties(menu_props)
        self._layoutmenu:show()
    else
        self._layoutmenu:hide()
    end
end

function tagmanager.taglistshow(self)
   local id =  awful.screen.focused().index
   if (self._tagmenu:is_visible()==false) then
        self:close()
        for i,t in pairs(self._tags) do
            if t == self._taglists[id].tag then
                self._tagmenu:select_item(i)
            end
        end

        local ws = capi.screen[id].geometry
        local x = 0
        if capi.screen[id] == capi.screen.primary then
            x = 70
        end

        local menu_props = {x=ws.x + x,
                            y=ws.y + awtk_theme.menubar.height + 1}
        self._tagmenu:set_properties(menu_props)
        self._tagmenu:show()
    else
        self._tagmenu:hide()
    end
end

function tagmanager.movetag(self, tag, screen)
    screen = screen or awful.screen.focused()
    local oldscreen = tag.screen

    -- If the specified tag is allocated to another screen, we need to move it,
    -- or if the tag no longer belongs to a screen.
    if oldscreen ~= screen or not oldscreen then
        -- Try to find a new tag to show on the previous screen if the currently
        -- selected tag is the one that was moved away.
        if oldscreen then
            local oldsel = oldscreen.selected_tag
            tag.screen = screen

            if oldsel == tag then
                -- The tag has been moved away. In most cases the tag history
                -- function will find the best match, but if we really want we can
                -- try to find a fallback tag as well.
                if not oldscreen.selected_tag then
                    local newtag = awful.tag.find_fallback(oldscreen)
                    if newtag then
                        newtag:view_only()
                    end
                end
            end
        end

        -- Also sort the tag in the taglist, by reapplying the index. This is just a nicety.
        return true
    end

    return false    
end


function tagmanager.gettag(self, i)
    return self._tags[i]
end

function tagmanager._toggle(self, modif, id)
    id = id or mouse.screen.index
    for i,tag in pairs(self._tags) do
        if tag == self._taglists[id].tag then
            if i + modif > table.getn(self._tags) then
                self:activate_tag(self._tags[1], id)
            elseif i + modif < 1 then
                self:activate_tag(self._tags[table.getn(self._tags)], id)
            else
                self:activate_tag(self._tags[i + modif], id)
            end
            return
        end
    end
end
function tagmanager.set(self, tags, layouts)
    self._layouts = layouts
    local maxtaglen = 0
    for k,q in pairs(tags) do
        tmptag = self:_addtag(q, k)
        table.insert(self._tags, tmptag)
        local len = string.len(q.name)
        if (maxtaglen < len) then
            maxtaglen = len
        end
    end
    local maxlaylen = 0
    for k,l in pairs(self._layouts) do
        table.insert(self._layoutmenudata,{l.name,
                                        l.icon,
                                        function ()
                                            tagmanager.activate_layout(self,k)
                                            tagmanager.close(self)
                                        end})
        local len = string.len(l.name)
        if (maxlaylen < len) then
            maxlaylen = len
        end
    end
    for _ in capi.screen do
        -- Widget for tags
        local icon = wibox.widget.imagebox()
        local textbox = wibox.widget
        {
            markup="",
            align="center",
            valign="center",
            font = awtk_theme.menubar.font.family,
            widget=wibox.widget.textbox,
            forced_width = (awtk_theme.menubar.font.width + 1) * maxtaglen,
        }
        local widget = wibox.layout.fixed.horizontal()
        widget.spacing = awtk_theme.menubar.spacing
        widget:add(icon)
        widget:add(textbox)
        widget:buttons(awful.util.table.join(
                    awful.button({ }, 1, function () tagmanager.taglistshow(self) end),
                    awful.button({ }, 4, function () tagmanager._toggle(self, 1) end),
                    awful.button({ }, 5, function () tagmanager._toggle(self, -1) end)
                    ))

	    local taglist = {widget = widget,
                         textbox = textbox,
                         icon = icon,
                         tag = nil
                        }
        table.insert(self._taglists, taglist)
        -- Widget for layouts
        local layicon = wibox.widget.imagebox()
        local laytextbox = wibox.widget
        {
            markup="",
            align="center",
            valign="center",
            font = awtk_theme.menubar.font.family,
            widget=wibox.widget.textbox,
            forced_width = (awtk_theme.menubar.font.width + 1) * maxlaylen,
        }
        local laywidget = wibox.layout.fixed.horizontal()
        laywidget.spacing = awtk_theme.menubar.spacing
        laywidget:add(layicon)
        laywidget:add(laytextbox)
        laywidget:buttons(awful.util.table.join(awful.button({ }, 1, function () tagmanager.layoutlistshow(self) end)))
        local layout = {widget = laywidget,
                        textbox = laytextbox,
                        icon = layicon}
        table.insert(self._layoutlists, layout)
    end

    --awful.button({ }, 1, function(t) tagmanager.viewonly(self,t) end),
    --awful.button({ modkey }, 1, awful.client.movetotag),
    self._tagmenu = menu.new(self._tagmenudata,{x=0,
                                                y=0,
                                                modkey=self._modkey,
                                                key=self._tagkey,
                                                enable_submenu = false})
    self._layoutmenu = menu.new(self._layoutmenudata,{x=0,
                                                      y=0,
                                                      modkey=self._modkey,
                                                      key=self._layoutkey,
                                                      enable_submenu = false})
    for s in capi.screen do
        for i,q in pairs(tags) do
            if q.screen and q.screen == s.index then
                self:activate_tag(self._tags[i], s.index)
                break
            end
        end
        self:activate_tag(self._tags[s.index], s.index)
    end
end
function tagmanager.new()
    local self = setmetatable({}, tagmanager)
    self._taglists = {}
    self._layouts = {}
    self._tags = {}
    self._layoutlists = {}
    self._modkey = nil
    self._tagmenudata = {}
    self._tagkey = 't'
    self._layoutmenudata = {}
    self._layoukey = 'l'
    self._tagmenu = {}
    self._layoutmenu = {}
    self._movingscreen = false
    return self
end

function tagmanager.set_modkey(self, modkey)
    self._modkey = modkey
end
function tagmanager.set_tagkey(self, key)
    self._modkey = nil
    self._tagkey = key
end

function tagmanager.set_layoutkey(self, key)
    self._layoutkey = key
end

function tagmanager.get_instance()
    return tagmanager_obj
end

function tagmanager.init(theme)
    awtk_theme = theme
end

local tagmanager_obj = tagmanager.new()
return tagmanager_obj
