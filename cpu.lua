local wibox = require("wibox")
local awful = require("awful")
--local awex = require("awex")
local math = { floor = math.floor }
local awtk = require("awtk")
local notify = awtk.notify
local cpu = {}
cpu.__index = cpu

function cpu.notify_hide(self)
    if self._notification ~= nil then
        if (self._notification:isVisible()) then
            self._notification:hide()
        end
    end
end

function cpu.notify_text(self)
    local text=""
    text="---------------------------------\n"
    text=text .. "<span color='white'><b>           PERFORMANCE:</b></span>\n"
    text=text .. "---------------------------------\n"
    for i, _ in ipairs(self._usage) do
        if (i > 1) then
            local corenum = tostring(i-1)
            text=text..' CPU '.. awtk.textutils.align_string( '#' .. tostring(corenum), 3) ..'\t\t'..awtk.textutils.colorize_number_len(100,0,self._usage[i],true,6)..' <span color="white">%</span>\n'
        end
    end
    text=text .. "---------------------------------\n"
    text=text .. "<span color='white'><b>           TEMPERATURE:</b></span>\n"
    text=text .. "---------------------------------\n"
    for i, _ in ipairs(self._temps) do
        text=text..' Core ' .. awtk.textutils.align_string( '#' .. i, 3) .. '\t\t'..awtk.textutils.colorize_number_len(110, 20, self._temps[i], true,5)..' <span color="white">°'..self._tempunit..'</span>\n'
    end
	text=text .. "---------------------------------\n"
    text=text .. "<span color='white'><b>             MEMORY:</b></span>\n"
    text=text .. "---------------------------------\n"
    text=text .. " Total memory:\t" .. awtk.textutils.align(self._mem[1], 13) .. ' <span color="white">MB</span>\n'
    text=text .. " Free memory:\t" .. awtk.textutils.colorize_number_len(self._mem[1], 0, self._mem[6],false,13) ..' <span color="white">MB</span>\n'
    text=text .. " Shared:\t" ..awtk.textutils.colorize_number_len(self._mem[1], 0, self._mem[4],true,13) .. ' <span color="white">MB</span>\n'
    text=text .. " Buffers:\t" .. awtk.textutils.colorize_number_len(self._mem[1], 0, self._mem[5],true,13) .. ' <span color="white">MB</span>\n'
    text=text .. " Total Swap:\t" .. awtk.textutils.align(self._mem[7], 13) .. ' <span color="white">MB</span>\n'
    text=text .. " Free Swap:\t" .. awtk.textutils.colorize_number_len(self._mem[7], 0, self._mem[9],false,13) .. ' <span color="white">MB</span>'


    return text
end

local function cpu_governon_list()
    local glist = {}
    local flist = {}
    local fd = io.popen("cpupower frequency-info")
    for _ = 1,6 do
        local tmp = fd:read()
        if tmp==nil then
    	    return nil
        end
    end
    _ = fd:read():gsub( "[0-9].-z", function(i) table.insert(flist,i) end)
    _ = string.match(fd:read(),':.-$'):gsub('%a+',function(i) table.insert(glist,i) end)
	fd:close()
    return {glist,flist}
end

function cpu.notify_show(self)
	self:update_resource()
    if (self._notification == nil) then
        self._notification = notify.new("devices/device_cpu",self:notify_text())
    end
    if (self._notification:isVisible() == false) then
        self._notification:show(screen.primary.index)
    end
end

function cpu.notify_keyb_show(self)
    if (self._notification ~= nil) and (self._notification:isVisible()) then
        self:notify_hide()
    else
        self:notify_show()
    end
end

local function readmeminfo ()
    local mem_info = {}
    local fd = io.popen("free -m")
    _ = fd:read("*line")
    for _ = 1,2
    do
        local str = fd:read("*line")
        if (str ~= nil) then
            str:gsub( "%d+", function(i) table.insert(mem_info, i) end)
        end
    end
    fd:close()
    return mem_info
end

function cpu.update_memory(self)
    -- Get MEM info
    self._mem = readmeminfo()
    local tmp =math.floor((self._mem[1]-self._mem[6])/self._mem[1] * 100);
    if (tmp ~= self._lastmem) then
        local text = awtk.textutils.colorize_number(100, 0, tmp, true) .. "%"
        self._textmem:set_markup(text)
    end
end

function cpu.update_cpu(self)
    local cpu_lines = {}
    -- Get CPU stats
    local f = io.open("/proc/stat")
    for line in f:lines() do
        if string.sub(line, 1, 3) ~= "cpu" then break end

        cpu_lines[#cpu_lines+1] = {}

        for i in string.gmatch(line, "[%s]+([^%s]+)") do
            table.insert(cpu_lines[#cpu_lines], i)
        end
    end

    f:close()
    for i = #self._total + 1, #cpu_lines do
        self._total[i]  = 0
        self._usage[i]  = 0
        self._active[i] = 0
    end

    for i, v in ipairs(cpu_lines) do
        -- Calculate totals
        local total_new = 0
        for j = 1, #v do
            total_new = total_new + v[j]
        end
        local active_new = total_new - (v[4] + v[5])

        -- Calculate percentage
        local diff_total  = total_new - self._total[i]
        local diff_active = active_new - self._active[i]

        if diff_total == 0 then
            diff_total = 1E-6
        end
        self._usage[i]      = math.floor((diff_active / diff_total) * 100)
        self._total[i]      = total_new
        self._active[i]     = active_new
    end
    local intusage = tonumber(self._usage[1])
    if (self._lastcpu ~= intusage) then
        self._lastcpu=intusage
        local text = awtk.textutils.colorize_number(100, 0, intusage, true) .. "%"
        self._textcpu:set_markup(text)
    end
end

function cpu.update_temperature(self)
	if (#self._temps == 0 or (self._notification ~= nil and self._notification:isVisible())) then
		local fd = io.popen("sensors")
	    local status = fd:read("*line")
		self._temps = {}
		self._crittemps = {}

		while (status ~= nil) do
			local words = {}
			if (status:find("Tctl")) then
				for word in status:gmatch("%w+") do
					table.insert(words, word)
                    print(word)
				end
				table.insert(self._temps, words[2] .. "." .. words [3])
				self._tempunit = words[4]
			end
			status = fd:read("*line")
		end
		fd:close()
	end
end

function cpu.update_resource(self)
   	self:update_cpu()
   	self:update_memory()
	self:update_temperature()
    if (self._notification ~= nil and self._notification:isVisible()) then
        self._notification:update_text(self:notify_text())
    end
end

function cpu.menu_show(self)
    self:notify_hide()
    -- get cpu power options
    local cpu_mhz_info = cpu_governon_list()
    if cpu_mhz_info == nil then
	    return
    end
    local governonmenu = {}
    local frequencymenu = {}
    local cpupowercmd = "doas cpupower frequency-set"
    for _,v in pairs(cpu_mhz_info[1]) do
        table.insert(governonmenu,{v,cpupowercmd .." -g " .. v})
    end
    for _,v in pairs(cpu_mhz_info[2]) do
        table.insert(frequencymenu,{v,cpupowercmd .." -f " .. v})
    end
    -- Build menus
    local menu = awful.menu({{"Policy",governonmenu},
                             {"Frequency",frequencymenu}})
    menu:toggle()

end

function cpu.atach(self)
    self._widget:connect_signal("mouse::enter", function () cpu:notify_show(self) end)
    self._widget:connect_signal("mouse::leave", function () cpu:notify_hide(self) end)
end

function cpu.widget(self)
	return self._widget
end

function cpu.cpulayout(self)
    return self._cpulayout
end

function cpu.memlayout(self)
    return self._memlayout
end

function cpu.new()
    local self = setmetatable({},cpu)
	self._usage  = {}
	self._total  = {}
	self._active = {}
	self._cpu = {}
	self._lastcpu = 0;
	self._mem = {}
	self._lastmem = 0;
	self._temps = {}
	self._crittemps = {}
	self._tempunit = ""
-- Initialize widget
	local cpu_icon=wibox.widget.imagebox()
	local memory_icon=wibox.widget.imagebox()
	cpu_icon:set_image(awtk.theme.get_icon(24,"devices/device_cpu"))
	memory_icon:set_image(awtk.theme.get_icon(24,"devices/device_mem"))

-- Initialize function tables

	self._notification=nil

	self._cpulayout = wibox.layout.fixed.horizontal()
    self._textcpu = wibox.widget
    {
        markup="",
        align="center",
        valign="center",
        font = awtk.theme.menubar.font.family,
        widget=wibox.widget.textbox,
        forced_width = (awtk.theme.menubar.font.width + 1) * 4,
    }
    self._textmem = wibox.widget
    {
        markup="",
        align="center",
        valign="center",
        font = awtk.theme.menubar.font.family,
        widget=wibox.widget.textbox,
        forced_width = (awtk.theme.menubar.font.width + 1) * 4,
    }
	self._cpulayout:add(cpu_icon)
    self._cpulayout:add(self._textcpu)
    self._cpulayout.spacing = awtk.theme.menubar.spacing
	self._memlayout = wibox.layout.fixed.horizontal()
	self._memlayout:add(memory_icon)
    self._memlayout:add(self._textmem)
    self._memlayout.spacing = awtk.theme.menubar.spacing
	self._memlayout:buttons(awful.util.table.join(
    	awful.button({ }, 1, function () cpu.menu_show(self) end)
	))
	awtk.looper:register(function() cpu.update_resource(self) end)
    self._cpulayout:connect_signal("mouse::enter", function () cpu.notify_show(self) end)
    self._cpulayout:connect_signal("mouse::leave", function () cpu.notify_hide(self) end)
    self._memlayout:connect_signal("mouse::enter", function () cpu.notify_show(self) end)
    self._memlayout:connect_signal("mouse::leave", function () cpu.notify_hide(self) end)
	return self
end



return cpu.new()
