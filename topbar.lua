local wibox = require("wibox")
local awful = require("awful")
local awtk = require("awtk")
local naughty = require("naughty")
local capi = {screen = screen,
              client = client}
local theme = awtk.theme;
local topbar = {}
local topbar_obj = nil;
topbar.__index = topbar
local app_icon = wibox.widget.imagebox()
local button_close = wibox.widget.imagebox()
local button_minimize = wibox.widget.imagebox()
local button_maximize = wibox.widget.imagebox()
local title = wibox.widget.textbox()

function topbar.get_titlebar(self,c)
    local tb = nil
    if (c ~= nil) then
        tb = awtk.titlebar.get(c)
        if (not tb) then
            tb = awtk.titlebar.new(c, function() topbar.maximize(self,c) end)           
        end
        tb:update()
        return tb
    end
    return nil
end

function topbar.maximize(self, c)
    if (c and c.dockable == false) then
        if (not (c.type == "splash" or c.type == "dock" or c.type == "desktop")) then
            local tb = self:get_titlebar(c)
            tb:maximize_active()
        end
    end
    self:_update_layouts(c)
end

function topbar.update(self, c)
    self:_update_layouts(c)
end

function topbar._update_layouts(self, c)
    for s in capi.screen do
        self._layouts[s.index]:reset()
        if c and s.index == c.screen.index and c.maximized and not c.minimized and not c.hidden then
            tb = awtk.titlebar.get(c)
            if (tb ~= nil) then
                tb:update()
                self:set_layout(tb:get_layout(), s.index)
            end
        else
            local clients = client.get(s.index)
            if #clients > 0 then
                for k in pairs(clients) do
                    if clients[k] ~= nil then
                        if (clients[k].maximized == true or
                            (awtk.tagmanager:get_screen_layout(s.index) == awful.layout.suit.max and
                             clients[k].first_tag == awtk.tagmanager:get_screen_layout(s.index) and
                             not clients[k].floating)) and
                            not clients[k].minimized and 
                            not clients[k].hidden then
                            local tb = self:get_titlebar(clients[k])
                            tb:update()
                            self:set_layout(tb:get_layout(), s.index)
                        end
                    end
                end
            end
        end
    end
end

function topbar.get_widget(self, i)
    return self._layouts[i]
end

function topbar.set_layout(self, layout, id)
    id = id or capi.client.focus.screen.index
	self._layouts[id].children = layout.children
    self._layouts[id]:emit_signal("widget::redraw_needed")
end

function topbar.new ()
    local self = setmetatable({}, topbar)
    self._layouts = {}
    for s in screen do
        local layout = wibox.layout.align.horizontal()
        layout.fill_space = false
        layout.expand = "none"
        table.insert(self._layouts, layout)
    end
    client.connect_signal("request::tag", function (c) topbar._update_layouts(self, c) end)
    client.connect_signal("request::geometry", function (c) topbar._update_layouts(self, c) end)
    tag.connect_signal("property::layout", function(t) topbar.update(self, client.focus) end)
    awtk.titlebar.connect_signal("unmanage", function(c) topbar._update_layouts(self, c) end)
    return self        
end

function topbar.get_instance()
    if (topbar_obj == nil) then
        topbar_obj = topbar.new()
    end
    return topbar_obj
end

return topbar.get_instance()
