local awtk = require("awtk")
local awful = require("awful")
local json = require("json")

local outfile = "/tmp/awesome/awkernel.tmp"
-- use stable, longterm or mainline
local myline = "stable"
local update_cycle = 3600 --seconds

local linux_icon = {
    "      _._     ",
    "     /<span color='white'>_ _</span>`.   ",
    "     <span color='white'>(.X.)</span>|   ",
    "     |<span color='yellow'>\\_/</span>'|   ",
    "     )<span color='white'>____</span>`\\  ",
    "    /<span color='white'>/_V _\\</span> \\ ",
    "   (<span color='white'>|  |  `</span>(<span color='yellow'>_</span>)",
    "  <span color='yellow'>/ \\</span>> '  <span color='white'>/</span><span color='yellow'>/ \\</span>",
    "  <span color='yellow'>\\  \\</span><span color='white'>\\__/</span><span color='yellow'>/  /</span>",
    "   <span color='yellow'>`-'    `-'</span> ",
    "              "
}

local kernelinfo={}
kernelinfo.__index = kernelinfo

local kernelinfo_obj = nil

local function format_date(str)
    if (str ~= "") then
        y,m,d = string.match(str,"(%d+)-(%d+)-(%d+)")
        local months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
        return (months[tonumber(m)]..", "..d.." "..y)
    end
end


function kernelinfo.get_data(self, data)
	self._kerneldata={}
	for vercount = 1, #data.releases do
		rline = data.releases[vercount].moniker
		rversion = data.releases[vercount].version
		rdate = data.releases[vercount].released.isodate
		local info = {
			line = rline,
			version = rversion,
		}
		table.insert(self._kerneldata,info)
		if (rline==myline) then
			if (self._latest_version_str == "") then
				self._latest_version_str = rversion
				self._latest_date=rdate
			end
			if (myline ~= "mainline") then
				compline, compver = string.match(rversion,"(%d+.%d+).(%d+)")
			else
				compline, compver = string.match(rversion,"(%d+.%d+)-rc(%d+)")
			end                
			if (compline == self._currline) then
				local tmp1 = tonumber(self._ver)
				local tmp2 = tonumber(compver)
				if (tmp1 == tmp2) then
					self._latest_version=true
					self._latest_date=rdate
				else
					self._latest_version=false
					self._latest_version_str=rversion
					self._latest_date=rdate
				end
			end
		end
	end
	return true
end

function kernelinfo.update(self, data)
	local lines = {}
	self._is_command_running = false
	if self:get_data(data) == true then
		if (#self._kerneldata ~= 0) then
			local str = "<span color='white'> Linux kernel info: </span>\n"
			table.insert(lines,str)
			if (self._latest_version == false) then
				str = "  Latest Version  = <span color='red'>" .. self._latest_version_str .. "</span> ("..format_date(self._latest_date)..")\n"
				table.insert(lines,str)
				str = "  Current Version = <span color='red'>" .. self._current .. "</span>\n"
				table.insert(lines,str)
			else    
				str = "  Current Version = <span color='green'>" .. self._current .. "</span> ("..format_date(self._latest_date)..")\n"
				table.insert(lines,str)
			end
			str = "\n"
			table.insert(lines,str);
			for i=1,math.floor(#self._kerneldata/2) do
				local tmpstr = string.format ("  %-10s",self._kerneldata[i].line)
				tmpstr = tmpstr .. " : " .. self._kerneldata[i].version
				tmpstr = string.format("%-23s", tmpstr)
				local tmpstr2 = string.format ("  %-10s",self._kerneldata[i+math.floor(#self._kerneldata/2)].line)
				tmpstr2 = tmpstr2 .. " : " .. self._kerneldata[i+math.floor(#self._kerneldata/2)].version .. "\n"
				str = tmpstr .. tmpstr2
				table.insert(lines,str)
			end
			if (#linux_icon > #lines) then
				local count = #linux_icon - #lines
				for i = 1,count do
					table.insert(lines,"\n")
				end
			end
			lines = awtk.textutils.combine_arrays({linux_icon,lines})
			str = "-----------------------------------------------------------------\n\n"
			for i, k in pairs(lines) do
				str = str .. k
			end
			if (self._desklet == nil) then
				ws = screen[1].geometry
				self._desklet = awtk.desklet.new(str, {x=ws.x, y = ws.y + 550 })
				self._desklet:show()
			else
				self._desklet:update_text(str)
			end
		end
	end
end


function kernelinfo.exec_command(self)
	if (self._is_command_running == false and self._updatecycle >= update_cycle) then

		self._is_command_running=true
		self._updatecycle=0
		awful.spawn.easy_async("curl --silent 'https://www.kernel.org/releases.json'",  function (stdout, stderr, reason, exitcode)
			if (exitcode == 0) then
				kernelinfo_obj:update(json.decode(stdout))
			end
		end)
	else
		self._updatecycle = self._updatecycle + 1
	end
end

function kernelinfo.show(self)
    self:exec_command()
    awtk.looper:register(function() kernelinfo_obj:exec_command(self) end)
end

function kernelinfo.new()
    local self = setmetatable({}, kernelinfo)
    self._is_command_running = false
    self._desklet = nil
    fd = io.popen("uname -r")
    self._current = fd:read("*line")
    if (myline ~= "mainline") then
        self._currline, self._ver = string.match(self._current,"(%d+.%d+).(%d+)")
    else
        self._currline, self._ver = string.match(self._current,"(%d+.%d+)-rc(%d+)")
    end
    fd:close()
    self._latest_version=false
    self._latest_version_str=""
    self._latest_date = ""
    self._kerneldata = {}
    self._updatecycle = update_cycle
    return self
end

local function get_instance()
    if (kernelinfo_obj == nil) then
        kernelinfo_obj = kernelinfo.new()
    end
    return kernelinfo_obj
end

return get_instance()
