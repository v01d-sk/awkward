local wibox = require("wibox")
local awtk = require("awtk")
local notify = awtk.notify
local textutils = awtk.textutils
local theme = awtk.theme
local battery_obj=nil;
local battery = {}
battery.__index = battery

local function file_exists(file)
  local f = io.open(file)
  if f then
      local s = f:read()
      f:close()
      f = s
  end
  return f ~= nil
end

local function lines_from(file)
  if not file_exists(file) then return {} end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end

local function read_line(file)
    return lines_from(file)[1]
end

function battery.notify_data(self)
    local bstr  = "/sys/class/power_supply/BAT0"
    local rate  = read_line(bstr .. "/current_now")
    local rem   = read_line(bstr .. "/charge_now")
    local remaining_time = ""
    local nrate  = tonumber(rate) or 1
    local nrem   = tonumber(rem)
    local intperc = tonumber(self._percentage)
    local strstate = ""
    if nrem ~= nil then
        local time_rat = nrem / nrate
        local hrs = math.floor(time_rat)
        local icon = ""
        if hrs < 0 then hrs = 0 elseif hrs > 23 then hrs = 23 end
        local min = math.floor((time_rat - hrs) * 60)
        if min < 0 then min = 0 elseif min > 59 then min = 59 end
        if (intperc < 20 ) then
            icon = "panel/battery-000"
        elseif (intperc < 40) then
            icon = "panel/battery-020"
        elseif (intperc < 60) then
            icon = "panel/battery-040"
        elseif (intperc < 80) then
            icon = "panel/battery-060"
        elseif (intperc < 98) then
            icon = "panel/battery-080"
        else
            icon = "panel/battery-100"
        end
        if self._state == "Charging" then
            icon = icon .. "-charging"
        end
        self._icon_path = icon

        if string.find(self._state,"Discharging") ~= nil then
            remaining_time = string.format("%02d:%02d", hrs, min)
        else
            remaining_time = "--:--"
        end
        strstate = string.format("% 15s", self._state)
    else
        remaining_time = "--:--"
        intperc = 0
        strstate = "Not connected"
        self._icon_path = "panel/battery-000-charging"
    end
    self._text = "------------------------------\n"
    self._text = self._text .. "<span color='white'><b>      POWER INFORMATIONS:</b></span>\n"
    self._text = self._text .. "------------------------------\n"
    self._text = self._text .. " Remaining Time:\t " .. remaining_time .. "\n"
    self._text = self._text .. " Charged on:\t\t " .. textutils.colorize_number(100, 0, intperc, false) .. " %\n"
    self._text = self._text .. " Status:\t" .. strstate
end

function battery.notify_hide(self)
    if self._notification and self._notification:isVisible() then
        self._notification:hide()
    end
end

function battery.notify_show(self)
    self:notify_data()
    if (self._notification == nil) then
        self._notification = notify.new(self._icon_path,self._text)
    end
    if (self._notification:isVisible() == false) then
        self._notification:show(screen.primary.index)
    end
end

function battery.update(self)
    local fdp = io.open("/sys/class/power_supply/BAT0/capacity")
    local fds = io.open("/sys/class/power_supply/BAT0/status")
    local icon = ""
    local intperc = 0
    if (fdp) then
        self._percentage = fdp:read("*all")
        self._state = fds:read("*all")
        self._state = self._state:gsub("%s+", "")
        fdp:close()
        fds:close()
        intperc = tonumber(self._percentage)
        if (self._percentage ~= self._last_percentage) or (self._state ~= self._last_state) then
            self._last_state=self._state
            self._last_percentage=self._percentage
            if (intperc < 20 ) then
                icon = "panel/battery-000"
            elseif (intperc < 40) then
                icon = "panel/battery-020"
            elseif (intperc < 60) then
                icon = "panel/battery-040"
            elseif (intperc < 80) then
                icon = "panel/battery-060"
            elseif (intperc < 95) then
                icon = "panel/battery-080"
            else
                icon = "panel/battery-100"
            end
            if self._state == "Charging" then
                icon = icon .. "-charging"
            end
            self._icon_path = theme.get_icon(24,icon)
            self._icon:set_image(self._icon_path)
        end
        local text = awtk.textutils.colorize_number(100, 0, intperc, false) .. "%"
        self._textbox:set_markup(text)
    else
        self._icon_path = theme.get_icon(24,"panel/battery-000-charging")
        self._icon:set_image(self._icon_path)
        self._textbox:set_markup("---")
    end
    if self._notification ~= nil and self._notification:isVisible() == true then
        self:notify_data()
        self._notification:update_icon(self._icon_path)
        self._notification:update_text(self._text)
    end
end

function battery.notify_keyb_show(self)
    if (self._notification and self._notification:isVisible()) then
        self:notify_hide()
    else
        self:notify_show(screen.primary.index)
    end
end

function battery.widget(self)
	return self._widget
end

function battery.new()
    local self = setmetatable({},battery)

	self._widget = wibox.layout.fixed.horizontal()
    self._widget.widget= layout
	self._icon=wibox.widget.imagebox()
	self._last_percentage=""
	self._percentage=""
	self._state=""
	self._last_state=""
	self._text=""
    self._textbox = wibox.widget
    {
        markup="",
        align="center",
        valign="center",
        font = awtk.theme.menubar.font.family,
        widget=wibox.widget.textbox,
        forced_width = (awtk.theme.menubar.font.width + 1) * 4,
    }
	self._icon_path=""
    self._widget.spacing = awtk.theme.menubar.spacing
	self._widget:add(self._icon)
    self._widget:add(self._textbox)
	self._notification = nil
	awtk.looper:register(function() battery_obj:update() end)
	battery_obj = self
    self._widget:connect_signal("mouse::enter", function () battery_obj:notify_show() end)
    self._widget:connect_signal("mouse::leave", function () battery_obj:notify_hide() end)
	return self
end

return battery.new()

