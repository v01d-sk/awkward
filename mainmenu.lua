local mainmenu = {}
mainmenu.__index = mainmenu

local awtk = require ("awtk")
local wibox = require ("wibox")
local awful = require ("awful")
local utils = require ("menubar.utils")
-- Set this variable to use custom menu directory e.g.
-- local menupaths = {awful.util.getdir("config").."/applications",}
-- Configured folder must contains .desktop files. You can also add
-- more items to menupaths to add custom items to system menu

local menupaths = {"/usr/share/applications", awful.util.getdir("config").."/applications"}

local sidebar = awtk.theme.default_iconset .. 'sideimg.png'

local power_menu = {{"Shutdown","apps/system-shutdown","doas /bin/loginctl poweroff"},
                    {"Reboot","apps/system-reboot","doas /bin/loginctl reboot"},
                    {"Suspend","apps/system-suspend", awful.util.getdir("config").."scripts/suspend.sh"}
                    }

local all_categories = {
    { app_type = "Application", name = "Applications",
                   icon_name = "categories/applications-all", use = true,
                   menu_data = {}},
    { app_type = "AudioVideo", name = "Multimedia",
                   icon_name = "categories/applications-multimedia", use = true,
                   menu_data = {}},
    { app_type = "Development", name = "Development",
                    icon_name = "categories/applications-development", use = true,
                    menu_data = {}},
    { app_type = "Education", name = "Education",
                  icon_name = "categories/applications-science", use = true,
                  menu_data = {}},
    { app_type = "Game", name = "Games",
              icon_name = "categories/applications-games", use = true,
              menu_data = {}},
    { app_type = "Graphics", name = "Graphics",
                 icon_name = "categories/applications-graphics", use = true,
                 menu_data = {}},
    { app_type = "Office", name = "Office",
               icon_name = "categories/applications-office", use = true,
               menu_data = {}},
    { app_type = "Network", name = "Internet",
                icon_name = "categories/applications-internet", use = true,
                menu_data = {}},
    { app_type = "Settings", name = "Settings",
                 icon_name = "categories/applications-system", use = true,
                 menu_data = {}},
    { app_type = "System", name = "System Tools",
               icon_name = "categories/applications-utilities", use = true,
               menu_data = {}},
    { app_type = "Emulator", name = "Emulation",
               icon_name = "categories/applications-emulation", use = true,
               menu_data = {}},
    { app_type = "Utility", name = "Accessories",
                icon_name = "categories/applications-accessories", use = true,
                menu_data = {}},
    { app_type = "Science", name = "Science",
                icon_name = "categories/applications-science", use = true,
                menu_data = {}},
    { app_type = "Electronics", name = "Electronics",
                icon_name = "panel/circuit", use = true,
                menu_data = {}},

}


function mainmenu.awesome_menu(self,menudata)
    self._awesome_menu = menudata
end


local function get_category_name_and_usage_by_type(app_type)
    for k, v in pairs(all_categories) do
        if app_type == v.app_type then
            return k, v
        end
    end
end

--- Parse a .desktop file.
-- @param file The .desktop file.
-- @return A table with file entries.
function mainmenu.parse(self, file)
    local program = { show = true, file = file }
    local desktop_entry = false

    -- Parse the .desktop file.
    -- We are interested in [Desktop Entry] group only.
    for line in io.lines(file) do
        if not desktop_entry and line == "[Desktop Entry]" then
            desktop_entry = true
        else
            if line:sub(1, 1) == "[" and line:sub(-1) == "]" then
                -- A declaration of new group - stop parsing
                break
            end

            -- Grab the values
            for key, value in line:gmatch("(%w+)%s*=%s*(.+)") do
                program[key] = value
            end
        end
    end

    -- In case [Desktop Entry] was not found
    if not desktop_entry then return nil end

	-- Don't show program if it should run in terminal
--	if program.Terminal and string.lower(program.Terminal) == "true" then 
--		program.show = false
--	end
    -- Don't show program if NoDisplay attribute is false
    if program.NoDisplay and string.lower(program.NoDisplay) == "true" then
        program.show = false
    end

    -- Only show the program if there is no OnlyShowIn attribute
    -- or if it's equal to utils.wm_name
    if program.OnlyShowIn ~= nil and not program.OnlyShowIn:match(utils.wm_name) then
        program.show = false
    end

    -- Look up for a icon.
    if program.Icon then
        program.icon_path = 'apps/' .. program.Icon
    end

    -- Split categories into a table. Categories are written in one
    -- line separated by semicolon.
    if program.Categories then
        program.categories = {}
        for category in program.Categories:gmatch('[^;]+') do
            table.insert(program.categories, category)
        end
    end

    if program.Exec then
        -- Substitute Exec special codes as specified in
        -- http://standards.freedesktop.org/desktop-entry-spec/1.1/ar01s06.html
        local cmdline = program.Exec:gsub('%%c', program.Name)
        cmdline = cmdline:gsub('%%[fuFU]', '')
        cmdline = cmdline:gsub('%%k', program.file)
        if program.icon_path then
            cmdline = cmdline:gsub('%%i', '--icon ' .. program.icon_path)
        else
            cmdline = cmdline:gsub('%%i', '')
        end
        program.cmdline = cmdline
    end

    return program
end

--- Parse a directory with .desktop files
-- @param dir The directory.
-- @param icons_size, The icons sizes, optional.
-- @return A table with all .desktop entries.
function mainmenu.parse_dir(self,dir)
    local programs = {}
    local files = io.popen('find '.. dir ..' -maxdepth 1 -name "*.desktop" 2>/dev/null')
    for file in files:lines() do
        local program = self:parse(file)
        if program then
            table.insert(programs, program)
        end
    end
	files:close()
    return programs
end

function mainmenu.generate(self)
    local result = {}
    for _, dir in ipairs(menupaths) do
        local entries = self:parse_dir(dir)
        for _, program in ipairs(entries) do
            -- Check whether to include program in the menu
            if program.show and program.Name and program.cmdline then
                local target_category = nil
                -- Check if the program falls at least to one of the
                -- usable categories. Set target_category to be the id
                -- of the first category it finds.
                if program.categories then
                    for _, category in pairs(program.categories) do
                        local cat_key, cat_use =
                            get_category_name_and_usage_by_type(category)
                        if cat_key and cat_use then
                            target_category = cat_use
                            local name = program.Name or ""
                            name = name:gsub("^%l", string.upper)
                            local cmdline = program.cmdline or ""
                            local icon = program.icon_path or nil
                            table.insert(target_category.menu_data, { name,icon,cmdline})
                        end
                    end
                end
            end
        end
    end
    if (self._awesome_menu ~= nil) then
        table.insert(self._data,{"Awesome","apps/awesome",self._awesome_menu})
        table.insert(self._data,{"-",nil,nil})
    end
    table.sort(all_categories, function(a,b) return a.name:upper() < b.name:upper() end)
    for v,k in pairs(all_categories) do
        if (table.getn(k.menu_data) ~= 0) then
            table.sort(k.menu_data,function(a,b) return a[1]:upper() < b[1]:upper() end)
            table.insert(self._data,{k.name,k.icon_name,k.menu_data})
        end
    end
    table.insert(self._data,{"-",nil,nil})
    table.insert(self._data,{"Run","apps/gmrun","gmrun"})
    table.insert(self._data,{"Power Menu","apps/system-shutdown",power_menu})
    --add shutdown menu
    self._generated=true
end

function mainmenu.show(self)
    if (self._menu == nil or self._menu:is_visible() == false) then
        local coords = mouse.coords()
        ws = screen[1].geometry
        local props = {mouse_close=true,
                         x=ws.x + 5,
                         y=ws.y + awtk.theme.menubar.height + 1,
                         modkey=self._modkey,
                         key=self._key,
                         side_bar = sidebar,
                         enable_submenu = true;
                     }
        if (self._generated == false) then
            self:generate()
            self._menu = awtk.menu.new(self._data,props)
        else
            self._menu:set_properties(props)
        end
        self._menu:show()
    else
        self:hide()
    end
end

function mainmenu.visible(self)
    return self._visible
end

function mainmenu.hide(self)
    if (self._menu:is_visible()) then
        self._menu:hide()
    end
end

function mainmenu.set_keys(self,modkey,key)
    self._modkey=modkey
    self._key=key
end

function mainmenu.new()
    local self = setmetatable({}, mainmenu)
    self._key = nil
    self._modkey = nil
    self._visible = false
    self._generated = false
    self._menu = nil
    self._props = {}
    self._data = {}
    self._awesome_menu = nil
    local icon = wibox.widget.imagebox()
    icon:set_image(awtk.theme.get_icon(24,"mainmenu"))
    self._widget = wibox.layout.fixed.horizontal() 
    local textbox = wibox.widget.textbox()
    self._widget.spacing = awtk.theme.menubar.spacing
    textbox:set_markup("<b>GENTOO</b>")
    textbox:set_font(awtk.theme.menubar.font.family)
    self._widget:add(icon)
    self._widget:add(textbox)
    self._widget:buttons(awful.util.table.join(
        awful.button({ }, 1, function () mainmenu.show(self) end)
    ))
    return self
end


function mainmenu.widget(self)
    return self._widget
end

return mainmenu.new()
