local gimpwm = {}
local awful = require ("awful")
local awtk = require ("awtk")
local wibox = require ("wibox")
local topbar = require ("topbar")

gimpwm.__index = gimpwm
gimpwm_obj = nil

function gimpwm.process_dock(self, c)
    c.maximized_horizontal=false
    c.maximized_vertical=false
    c.fullscreen=false
    c.above=true
    c.skip_taskbar=true
    c.floating=false
    c.type="desktop"
    awful.titlebar.hide(c)
    if (c.screen == 1) then
        ypos=awtk.theme.menubar.height 
    else
        ypos=0
    end
    c:geometry({x = screen[c.screen].geometry.width - 284,
                y = ypos,
                width=260,
                height= screen[c.screen].geometry.height-ypos})
    if (self._docker == nil) then
        docker_config = {position = "right",
                         size="fixed",
                         iconsize=20,
                         tags=c:tags(),
                         screen=c.screen
                        }
        self._docker = awtk.docker.new(docker_config)
    end
    self._docker:add(c)
end

local function converttext(text)
    local retstring = "<span color='white'>"
    for i=1, #text do
        local c = text:sub(i,i)
        retstring = retstring .. c .."\n"
    end
    retstring = retstring .. "</span>"
    return retstring
end

function gimpwm.process_toolbox(self, c)
    if (c.screen == 1) then
        ypos=awtk.theme.menubar.height 
    else
        ypos=0
    end
    c.maximized_horizontal=false
    c.maximized_vertical=false
    c.fullscreen=false
    c.skip_taskbar=true
    c.floating=false
    c.above=true
    awful.titlebar.hide(c)
    awful.client.dockable.set(c,true)
    c:geometry({x = screen[c.screen].geometry.x + 18,
                y = ypos,
                width=72,
                height= screen[c.screen].geometry.height-ypos})
    local mainlayout = wibox.layout.align.vertical()
    local textbox = wibox.widget.textbox()
    textbox:set_valign("center")
    textbox:set_align("center")
    textbox:set_markup(converttext(c.name))
    mainlayout:set_middle(textbox)
    self._toolbox = awtk.dock.new(c, mainlayout, {x = screen[c.screen].geometry.x,
                                         y = ypos,
                                         width=20,
                                         height= screen[c.screen].geometry.height-ypos})
    self._toolbox:show()
end

function gimpwm.process_mainwindow(self,c)
    if (c.screen == 1) then
        ypos=awtk.theme.menubar.height 
    else
        ypos=awtk.theme.titlebar.geometry.height 
    end
    c.maximized_horizontal=false
    c.maximized_vertical=false
    c.fullscreen=false
    c.skip_taskbar=true
    c.floating=false
    awful.titlebar.hide(c)
    awful.client.dockable.set(c,false)
    c:geometry({x = screen[c.screen].geometry.x + 20,
                y = ypos,
                width=screen[c.screen].geometry.width - 44,
                height= screen[c.screen].geometry.height-ypos})
    topbar:force_titlebar(c)
end

function gimpwm.process_client(self, c)
    if (c.role == "gimp-dock") then
        self:process_dock(c)
    elseif (c.role == "gimp-toolbox") then
        self:process_toolbox(c)
    elseif (c.role == "gimp-image-window") then
        self:process_mainwindow(c)
    end
end
function gimpwm.winfunc()
    wm = gimpwm.get_instance()
    if (wm._counter==0) then
        for _,k in pairs(wm._clients) do
            wm:process_client(k)
            print("Processing")
        end
        wm._startup=false
        awtk.looper:unregister(wm._loopid)
    else
        wm._counter = wm._counter -1
    end
end

function gimpwm.process_win(self, c)
    if (c.role == "gimp-startup") then
        self._startup=true;
        self._loopid=awtk.looper:register(function() gimpwm.winfunc() end)
    else
        table.insert(self._clients,c)
        if (self._startup==false) then
            self:process_client(c)
        end
    end
end

function gimpwm.new()
    local self = setmetatable({}, gimpwm)
    self._docker=nil
    self._loopid=0;
    self._toolbox={}
    self._clients={}
    self._active=nil
    self._counter=15
    self._startup=false;
    return self
end

function gimpwm.get_instance()
    if (gimpwm_obj == nil) then
        gimpwm_obj = gimpwm.new()
    end
    return gimpwm_obj
end

return gimpwm.get_instance()
